package year2018.day19Processor2;

import year2018.Day16.operations.*;

public class OperationFactory {

    public static Operation produce(String name, OperationArguments arguments){
        switch(name){
            case "addi":
                return new AdditionImmediate(arguments);
            case "addr":
                return new AdditionRegister(arguments);
            case "seti":
                return new AssignmentImmediate(arguments);
            case "setr":
                return new AssignmentRegister(arguments);
            case "andi":
                return new BitwiseAndImmediate(arguments);
            case "andr":
                return new BitwiseAndRegister(arguments);
            case "ori":
                return new BitwiseOrImmediate(arguments);
            case "orr":
                return new BitwiseOrRegister(arguments);
            case "eqir":
                return new EqualityTestingImmediateRegister(arguments);
            case "eqri":
                return new EqualityTestingRegisterImmediate(arguments);
            case "eqrr":
                return new EqualityTestingRegisterRegister(arguments);
            case "gtir":
                return new GreaterThanTestingImmediateRegister(arguments);
            case "gtri":
                return new GreaterThanTestingRegisterImmediate(arguments);
            case "gtrr":
                return new GreaterThanTestingRegisterRegister(arguments);
            case "muli":
                return new MultiplicationImmediate(arguments);
            case "mulr":
                return new MultiplicationRegister(arguments);
        }
        throw new RuntimeException();
    }

}
