package year2018.day19Processor2;

import util.FilesReader;
import util.GlobalConstatns;
import year2018.Day16.Registers;
import year2018.Day16.operations.Operation;
import year2018.Day16.operations.OperationArguments;

import java.util.ArrayList;
import java.util.List;

public class Day19 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2018,"day19Processor2");
        List<String> lines = FilesReader.getLines(path);

        int operationPointer = Integer.parseInt(lines.get(0).split(" ")[1]);

        Registers registers = new Registers(6);
        registers.set(0, 1);

        List<Operation> operations = new ArrayList<>();

        for(int i = 1; i < lines.size(); i++){
            String[] tokens = lines.get(i).split(" ");
            int op1 = Integer.parseInt(tokens[1]);
            int op2 = Integer.parseInt(tokens[2]);
            int op3 = Integer.parseInt(tokens[3]);
            OperationArguments arguments = new OperationArguments(op1, op2, op3);
            operations.add(OperationFactory.produce(tokens[0], arguments));
        }


        registers.set(1, 1);
        registers.set(2, 10551340);
        registers.set(3, 3);
        registers.set(4, 1);
        registers.set(5, 1);


        task2();


    }

    private static void task2() {
        int c = 10551340;
        int a = 0;
        for (int i = 1; i <= c; i++) {
            if (c%i == 0) {
                a += i;
                System.out.println(a);
            }
        }
    }

    private static void task1(int operationPointer, Registers registers, List<Operation> operations) {
        while (registers.get(operationPointer) >= 0 && registers.get(operationPointer) < operations.size()){
            System.out.println(registers.getRegisters());
            Operation currentOperation = operations.get((int) registers.get(operationPointer));
            currentOperation.compute(registers);
            registers.set(operationPointer, registers.get(operationPointer) + 1);
        }
    }

}
