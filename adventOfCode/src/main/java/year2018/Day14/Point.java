package year2018.Day14;

import javafx.scene.paint.Color;
import lombok.Data;

@Data
public class Point {
    private int x;
    private int y;
    private Color color;

    public Point(int index, int episode, int value){
        x = index;
        y = episode;
        color = Color.color(0, 0, value/9.0);
    }
}
