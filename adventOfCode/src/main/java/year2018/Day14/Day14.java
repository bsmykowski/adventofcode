package year2018.Day14;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;

public class Day14 extends Application {
    private static final double SCALE = 1;
    private static final double WIDTH = 900;
    private static final double HEIGHT = 900;

    @Override
    public void start(Stage primaryStage) throws Exception {


        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(WIDTH * SCALE, HEIGHT * SCALE);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root, WIDTH * SCALE, HEIGHT * SCALE));
        primaryStage.show();

        secondTask();
    }

    private void secondTask() {
        List<Integer> sequence = Arrays.asList(9, 9, 0, 9, 4, 1);
        Episode episode = new Episode(Arrays.asList(3, 7), 0, 1);

        while(!episode.sequenceExist(sequence, 0)
            && !episode.sequenceExist(sequence, -1)){
            episode.nextEpisode();
        }
        System.out.println(episode.getSequencePosition(sequence));
    }

    private void firstTask() {
        int scoreIndex = 990941;
        Episode episode = new Episode(Arrays.asList(3, 7), 0, 1);

        for(int i = 0; i < scoreIndex*2; i++){
            episode.nextEpisode();
            if(i%10000==0)
                System.out.println(i/10000);
        }
        System.out.println(episode.scores(scoreIndex));
    }

    private void drawCanvas(List<Point> points, Canvas canvas) {
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        for (Point point : points) {
            graphicsContext2D.setFill(point.getColor());
            graphicsContext2D.fillRect(point.getX() * SCALE, point.getY() * SCALE, SCALE, SCALE);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}
