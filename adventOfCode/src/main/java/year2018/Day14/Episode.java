package year2018.Day14;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Episode {
    private List<Integer> values = new ArrayList<>();
    private int firstIndex;
    private int secondIndex;

    public Episode(List<Integer> startingScores, int firstIndex, int secondIndex){
        this.firstIndex = firstIndex;
        this.secondIndex = secondIndex;
        values.addAll(startingScores);
    }

    public String scores(int index){
        StringBuilder score = new StringBuilder();
        for(int i = 0; i < 10; i++){
            score.append(values.get(index + i));
        }
        return score.toString();
    }

    public int getSequencePosition(List<Integer> sequence){
        if(sequenceExist(sequence, 0)){
            return values.size()-sequence.size();
        }
        if(sequenceExist(sequence, -1)){
            return values.size()-sequence.size() - 1;
        }
        return -1;
    }

    public boolean sequenceExist(List<Integer> sequence, int faze){
        if(values.size() + faze < sequence.size())
            return false;
        int length = sequence.size();
        for(int i = 0; i < length; i++){
            if(!values.get(values.size() - length+i+faze).equals(sequence.get(i))){
                return false;
            }
        }
        return true;
    }

    public void nextEpisode(){
        int firstScore = values.get(firstIndex);
        int secondScore = values.get(secondIndex);
        int sum = firstScore + secondScore;
        if(sum > 9){
            values.add(1);
            values.add(sum%10);
        }else {
            values.add(sum);
        }
        firstIndex = (firstIndex+1+firstScore)%values.size();
        secondIndex = (secondIndex+1+secondScore)%values.size();
    }

    public void print() {
        System.out.println(values);
    }

}
