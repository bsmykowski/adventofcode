package year2018.Day12;

import java.util.ArrayList;
import java.util.List;

public class Generation {
    private String pots;
    private int zeroPotIndex;

    public Generation(String pots, int zeroPotIndex){
        this.pots = pots;
        this.zeroPotIndex = zeroPotIndex;
    }

    public void nextGeneration(Rules rules){
        StringBuilder currentState = new StringBuilder("....." + pots + ".....");
        StringBuilder newState = new StringBuilder();
        for(int i = 2; i < currentState.length() - 2; i++){
            newState.append(rules.getResult(currentState.substring(i-2, i+3)));
        }
        int start = newState.indexOf("#");
        int end = newState.lastIndexOf("#") + 1;
        pots = newState.substring(start, end);
        zeroPotIndex = zeroPotIndex - start + 3;
    }

    public List<Point> getPoints(int generation){
        List<Point> points = new ArrayList<>();
        for(int i = 0; i < pots.length(); i++){
            if(pots.charAt(i) == '#'){
                points.add(new Point(i+5-zeroPotIndex, generation));
            }
        }
        return points;
    }

    public void print(){
        for(int i = 0; i < 5-zeroPotIndex; i++){
            System.out.print(" ");
        }
        System.out.println(pots);
    }

    public int getScore(){
        int counter = -zeroPotIndex;
        int result = 0;
        for(int i = 0; i < pots.length(); i++){
            if(pots.charAt(i) == '#'){
                result += counter;
            }
            counter++;
        }
        return result;
    }

}
