package year2018.Day12;

import lombok.Getter;

public class Rule {
    @Getter
    private String accepts;
    private String result;

    public Rule(String ruleString){
        String[] tokens = ruleString.split(" ");
        accepts = tokens[0];
        result = tokens[2];
    }

    public boolean check(String pattern){
        return pattern.equals(accepts);
    }

    public String getResult(){
        return result;
    }
}
