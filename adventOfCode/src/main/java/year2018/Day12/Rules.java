package year2018.Day12;

import java.util.HashMap;
import java.util.List;

public class Rules {
    private HashMap<String, Rule> rules;

    public Rules(List<String> rulesString){
        rules = new HashMap<>();
        for(String ruleString: rulesString){
            Rule rule = new Rule(ruleString);
            rules.put(rule.getAccepts(), rule);
        }
    }

    public String getResult(String pattern){
        if(!rules.containsKey(pattern))
            return ".";
        return rules.get(pattern).getResult();
    }

}
