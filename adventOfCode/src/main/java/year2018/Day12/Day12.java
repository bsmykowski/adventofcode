package year2018.Day12;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import util.FilesReader;
import util.GlobalConstatns;

import java.util.ArrayList;
import java.util.List;

public class Day12 extends Application {

    private static final double SCALE = 1;
    private static final double WIDTH = 900;
    private static final double HEIGHT = 900;

    @Override
    public void start(Stage primaryStage) throws Exception {
        String path = GlobalConstatns.getPath(2018, 12);
        List<String> lines = FilesReader.getLines(path);

        List<List<Point>> points = new ArrayList<>();

        String initState = lines.get(0).split(" ")[2];

        Generation generation = new Generation(initState, 0);
        points.add(generation.getPoints(1));

        Rules rules = new Rules(lines.subList(2, lines.size()));

        for(int i = 0; i < 500; i++){
            generation.nextGeneration(rules);
            points.add(generation.getPoints(i+1));
            int score = generation.getScore();
            System.out.println(i + 1 + " " + score);
        }

        System.out.println((50000000000L - 500) *42 + 21428);

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(WIDTH * SCALE, HEIGHT * SCALE);
        root.getChildren().add(canvas);
        drawCanvas(points, canvas);
        primaryStage.setScene(new Scene(root, WIDTH * SCALE, HEIGHT * SCALE));
        primaryStage.show();

    }

    private void drawCanvas(List<List<Point>> mainPoints, Canvas canvas) {
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        graphicsContext2D.setFill(Color.WHITE);
        graphicsContext2D.fillRect(0, 0, WIDTH*SCALE, HEIGHT*SCALE);
        for (List<Point> row : mainPoints) {
            for (Point point : row) {
                graphicsContext2D.setFill(point.getColor());
                graphicsContext2D.fillRect(point.getX() * SCALE, point.getY() * SCALE, SCALE, SCALE);
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}
