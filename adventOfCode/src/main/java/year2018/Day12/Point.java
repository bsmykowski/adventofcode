package year2018.Day12;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Point {
    private int x = 0;
    private int y = 0;
    private Color color;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
        color = Color.RED;
    }

}
