package year2018.Day3;

import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Data
public class FabricField {
    private FabricFieldState state;
    @Getter
    private List<Integer> takenBy;

    public FabricField(){
        state = FabricFieldState.FREE;
        takenBy = new ArrayList<>();
    }

    public void addRectangle(Integer id){
        takenBy.add(id);
    }

    public void addTakenBy(Rectangle rectangle){
        takenBy.add(rectangle.getId());
    }

}
