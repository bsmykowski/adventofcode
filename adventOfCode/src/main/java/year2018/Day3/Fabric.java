package year2018.Day3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Fabric {
    private int width;
    private int height;
    private List<List<FabricField>> fabric;
    private Map<Integer, Rectangle> rectangles;

    public Fabric(int width, int height){
        this.height = height;
        this.width = width;
        fabric = new ArrayList<>();
        for(int i = 0; i < width; i++) {
            List<FabricField> column = new ArrayList<>();
            for(int j = 0; j < height; j++) {
                column.add(new FabricField());
            }
            fabric.add(column);
        }
        this.rectangles = new HashMap<>();
    }

    public void addRectangle(Rectangle rectangle){
        rectangles.put(rectangle.getId(), rectangle);
        for(int i = 0; i < rectangle.getWidth(); i++) {
            for(int j = 0; j < rectangle.getHeight(); j++) {
                Point point = getPointOnFabric(rectangle, i, j);
                if(getFieldState(point) == FabricFieldState.FREE) {
                    setFabricFieldState(point, FabricFieldState.TAKEN);
                } else if(getFieldState(point) == FabricFieldState.TAKEN) {
                    setFabricFieldState(point, FabricFieldState.OVERLAPPING);
                    rectangle.setThatOverlaps();
                    List<Integer> takenByList = getField(point).getTakenBy();
                    for(Integer id: takenByList){
                        if(rectangles.get(id) != null) {
                            rectangles.get(id).setThatOverlaps();
                        }
                    }
                } else if(getFieldState(point) == FabricFieldState.OVERLAPPING) {
                    rectangle.setThatOverlaps();
                    List<Integer> takenByList = getField(point).getTakenBy();
                    for(Integer id: takenByList){
                        if(rectangles.get(id) != null) {
                            rectangles.get(id).setThatOverlaps();
                        }
                    }
                }
                getField(point).addTakenBy(rectangle);
            }
        }
    }

    private Point getPointOnFabric(Rectangle rectangle, int i, int j) {
        Point point = new Point(0,0);
        point.addPoint(rectangle.getStartingPoint());
        point.addPoint(new Point(i, j));
        return point;
    }

    public FabricField getField(Point point){
        return fabric.get(point.getX()).get(point.getY());
    }

    public FabricFieldState getFieldState(Point point){
        return getField(point).getState();
    }

    public void setFabricFieldState(Point point, FabricFieldState state){
        fabric.get(point.getX()).get(point.getY()).setState(state);
    }

    public int getNotOverlappingId(){
        for (Rectangle rectangle : rectangles.values()) {
            if (!rectangle.getOverlaps()) {
                return rectangle.getId();
            }
        }
         return -1;
    }

    public int getOverlappingAmount(){
        int overlappingCounter = 0;
        for(int i = 0; i < width; i++) {
            for(int j = 0; j < height; j++) {
                if(getFieldState(new Point(i, j)) == FabricFieldState.OVERLAPPING){
                    overlappingCounter++;
                }
            }
        }
        return overlappingCounter;
    }

}
