package year2018.Day3;

import lombok.Getter;

@Getter
public class Rectangle {

    private int id;
    private int width;
    private int height;
    private Point startingPoint;
    private Boolean overlaps = false;

    public Rectangle(String line){
        String[] tokens = line.split(" ");
        this.id = Integer.parseInt(tokens[0].substring(1));

        String positionToken = tokens[2].substring(0, tokens[2].length() - 1);
        String[] positionsTokens = positionToken.split(",");
        this.startingPoint = new Point(Integer.parseInt(positionsTokens[0]), Integer.parseInt(positionsTokens[1]));

        String[] sizeTokens = tokens[3].split("x");
        this.width = Integer.parseInt(sizeTokens[0]);
        this.height = Integer.parseInt(sizeTokens[1]);
    }

    public void setThatOverlaps(){
        overlaps = true;
    }

}
