package year2018.Day3;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.List;

public class Day3 {

    public static void main(String[] args) {

        String path = GlobalConstatns.getPath(2018, 3);
        List<String> lines = FilesReader.getLines(path);

        Fabric fabric = new Fabric(1000, 1000);

        for(String line:lines){
            Rectangle rectangle = new Rectangle(line);
            fabric.addRectangle(rectangle);
        }

        System.out.println(fabric.getNotOverlappingId());

    }

}
