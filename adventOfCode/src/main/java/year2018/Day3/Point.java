package year2018.Day3;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Point {
    private int x = 0;
    private int y = 0;

    public void addPoint(Point point){
        this.x += point.getX();
        this.y += point.getY();
    }
}
