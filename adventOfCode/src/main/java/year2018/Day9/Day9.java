package year2018.Day9;

import java.util.HashMap;
import java.util.Map;

public class Day9 {

    public static void main(String[] args) {

        int playersAmount = 430;
        int maxValue = 7158800;
        Map<Integer, Long> players = new HashMap<>();
        NodesList marbles = new NodesList();

        for(int i = 0; i < playersAmount; i++){
            players.put(i, 0L);
        }

        for(int i = 1; i <= maxValue; i++){
            if(i%23 == 0){
                marbles.moveLeft(7);
                long oldValue = players.get(i % playersAmount);
                long newValue = oldValue + i + marbles.getCurrent().getValue();
                players.replace(i%playersAmount, newValue);
                marbles.removeCurrent();
            } else {
                Node newNode = new Node();
                newNode.setValue(i);
                marbles.moveRight();
                marbles.addAfterCurrent(newNode);
            }
            if(i%10000 == 0){
                System.out.println(i/10000);
            }
        }

        long max = 0;
        for(Map.Entry<Integer, Long> entry:players.entrySet()){
            if(entry.getValue() > max){
                max = entry.getValue();
            }
        }
        System.out.println(max);

    }

}
