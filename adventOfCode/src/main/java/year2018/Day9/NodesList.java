package year2018.Day9;

import lombok.Data;

@Data
public class NodesList {
    private Node root;
    private Node current;
    private int size;

    public NodesList(){
        current = new Node();
        current.setAfter(current);
        current.setBefore(current);
        root = current;
        size = 0;
    }

    public void addAfterCurrent(Node node){
        Node next = current.getAfter();
        current.setAfter(node);
        node.setBefore(current);
        node.setAfter(next);
        next.setBefore(node);
        current = node;
        size++;
    }

    public void moveLeft(int steps){
        for(int i = 0; i< steps; i++){
            moveLeft();
        }
    }

    public void moveLeft(){
        current = current.getBefore();
    }

    public void moveRight(int steps){
        for(int i = 0; i< steps; i++){
            moveRight();
        }
    }

    public void moveRight(){
        current = current.getAfter();
    }

    public void removeCurrent(){
        Node next = current.getAfter();
        Node before = current.getBefore();
        before.setAfter(next);
        next.setBefore(before);
        current = next;
        size--;
    }

    public void print(){
        Node next = root;
        for(int i = 0; i < size; i++){
            System.out.print(next.getValue() + " ");
            next = next.getAfter();
        }
        System.out.println(next.getValue());
    }
}
