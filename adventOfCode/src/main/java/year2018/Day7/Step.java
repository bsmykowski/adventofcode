package year2018.Day7;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Step {

    public static final int ADDITIONAL_TIME = 60;
    private Character id;
    private List<Character> previousSteps;
    private Boolean ready;
    private int workTime;
    private int leftTime;

    public Step(Character id){
        this.id = id;
        ready = false;
        previousSteps = new ArrayList<>();
        workTime = ADDITIONAL_TIME + id - 'A' + 1;
        leftTime = workTime;
    }

    public void addPrevious(Character previous){
        previousSteps.add(previous);
    }

    public void tick(){
        if(leftTime > 0) {
            leftTime--;
        }
    }

    public boolean isDone(){
        return leftTime == 0;
    }

    public boolean isReady(List<Character> completed){
        for (Character previous: previousSteps){
            if(!completed.contains(previous)){
                return false;
            }
        }
        return true;
    }
}
