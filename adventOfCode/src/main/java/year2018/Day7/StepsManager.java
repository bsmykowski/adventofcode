package year2018.Day7;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class StepsManager {

    private Map<Character, Step> steps;
    private List<Processor> processors;
    private List<Character> doneSteps = new ArrayList<>();
    private List<Character> readySteps = new ArrayList<>();
    private List<Character> toDoSteps = new ArrayList<>();
    private int time;

    public StepsManager(int numberOfProcessors, Map<Character, Step> steps){
        this.steps = steps;
        processors = new ArrayList<>();
        for(int i = 0; i < numberOfProcessors;i++){
            processors.add(new Processor());
        }
        time = 0;
    }

    public void doSteps(){
        prepareReadyAndToDoSteps();
        while(!readySteps.isEmpty() || !toDoSteps.isEmpty() || processorsAreBusy()){
            assignStepsToFreeProcessors();

            printLogs();

            tickProcessors();
            endDoneSteps();
            updateReadySteps();
            removeReadyStepsFromToDoList();
            time++;
        }
        printDoneSteps();
        System.out.print(time);
    }

    private void printLogs() {
        System.out.printf("%03d ", time);
        for (Processor processor:processors){
            if(processor.getActualStep() != null)
                System.out.print(processor.getActualStep().getId());
            else
                System.out.print(" ");
        }
        System.out.print("     ");
        for(Character done:doneSteps) {
            System.out.print(done);
        }
        System.out.println();
    }

    private void printDoneSteps() {
        for(Character done:doneSteps) {
            System.out.print(done);
        }
        System.out.println();
    }

    private void prepareReadyAndToDoSteps() {
        for(Map.Entry<Character, Step> step:steps.entrySet()){
            if(step.getValue().getPreviousSteps().isEmpty()){
                readySteps.add(step.getKey());
            } else {
                toDoSteps.add(step.getKey());
            }
        }
        readySteps.sort(Character::compareTo);
    }

    private void removeReadyStepsFromToDoList() {
        for(Character ready:readySteps) {
            toDoSteps.remove(ready);
        }
    }

    private void updateReadySteps() {
        for(Character toDo:toDoSteps) {
            if (steps.get(toDo).isReady(doneSteps)) {
                readySteps.add(toDo);
            }
        }
        readySteps.sort(Character::compareTo);
    }

    private void endDoneSteps() {
        for(Processor processor:processors) {
            if (!processor.isBusy() && processor.getLastDoneStep() != null){
                doneSteps.add(processor.getLastDoneStep().getId());
                //System.out.println("Time " + time + " done " + processor.getLastDoneStep().getId());
                processor.setLastDoneStep(null);
            }
        }
    }

    private void tickProcessors() {
        for(Processor processor:processors){
            processor.tick();
        }
    }

    private void assignStepsToFreeProcessors() {
        List<Processor> freeProcessors = getFreeProcessors();
        int numberOfReadySteps = readySteps.size();
        for(int i = 0; i<freeProcessors.size() && i < numberOfReadySteps; i++) {
            //System.out.println("Time: " + time + " processor: " + freeProcessors.get(i).getId() + " step id: " + readySteps.get(0));
            freeProcessors.get(i).setActualStep(steps.get(readySteps.get(0)));
            readySteps.remove(0);
        }
    }

    private List<Processor> getFreeProcessors() {
        List<Processor> freeProcessors = new ArrayList<>();
        for(Processor processor : processors){
            if(!processor.isBusy()){
                freeProcessors.add(processor);
            }
        }
        return freeProcessors;
    }

    private boolean processorsAreBusy(){
        for(Processor processor: processors){
            if(processor.isBusy())
                return true;
        }
        return false;
    }

}
