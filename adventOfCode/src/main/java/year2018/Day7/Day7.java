package year2018.Day7;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.*;

public class Day7 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2018, 7);
        List<String> lines = FilesReader.getLines(path);

        Map<Character, Step> steps = new HashMap<>();

        for (String line:lines){
            String[] tokens = line.split(" ");
            Character firstId = tokens[1].charAt(0);
            Character secondId = tokens[7].charAt(0);
            Step firstStep = steps.getOrDefault(firstId, new Step(firstId));
            Step secondStep = steps.getOrDefault(secondId, new Step(secondId));
            steps.putIfAbsent(firstId, firstStep);
            steps.putIfAbsent(secondId, secondStep);
            secondStep.addPrevious(firstId);
        }

        StepsManager stepsManager = new StepsManager(5, steps);

        stepsManager.doSteps();

    }

}
