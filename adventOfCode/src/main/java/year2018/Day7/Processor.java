package year2018.Day7;

import lombok.Data;

@Data
public class Processor {
    public static int ID_COUNTER = 0;
    private int id;
    private Step actualStep;
    private Step lastDoneStep;

    public Processor(){
        actualStep = null;
        lastDoneStep = null;
        id = generateId();
    }

    public void tick(){
        if(actualStep != null) {
            actualStep.tick();
            if (actualStep.isDone()) {
                lastDoneStep = actualStep;
                actualStep = null;
            }
        }
    }

    public void startStep(Step step){
        actualStep = step;
    }

    public boolean isBusy(){
        return actualStep != null;
    }

    private static int generateId(){
        return ID_COUNTER++;
    }

}
