package year2018.Day1;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day1 {

    public static void main(String[] args) {

        String path = GlobalConstatns.getPath(2018, 1);
        List<String> lines = FilesReader.getLines(path);
        Set<Integer> reachedCounters = new HashSet<>();

        int counter = 0;

        int i = 0;
        while(!reachedCounters.contains(counter)){
            reachedCounters.add(counter);
            int number = Integer.parseInt(lines.get(i%lines.size()));
            counter += number;
            i++;
        }

        System.out.println(counter);

    }

}
