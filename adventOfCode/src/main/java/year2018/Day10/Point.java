package year2018.Day10;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Point {
    private int x = 0;
    private int y = 0;
    private Color color;
    private int velocityX;
    private int velocityY;

    public Point(String pointString){
        String[] tokens = pointString.split("<");
        String[] positionTokens = tokens[1].split(">")[0].split(",");
        String[] velocityTokens = tokens[2].split(">")[0].split(",");

        x = Integer.parseInt(positionTokens[0].trim());
        y = Integer.parseInt(positionTokens[1].trim());
        velocityX = Integer.parseInt(velocityTokens[0].trim());
        velocityY = Integer.parseInt(velocityTokens[1].trim());
    }

    public Point(int x, int y, int velocityX, int velocityY){
        this.x = x;
        this.y = y;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
    }

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public void tick(){
        x += velocityX;
        y += velocityY;
    }

    public double distance(Point point){
        return Math.sqrt(Math.pow(point.x - x, 2) + Math.pow(point.y - y, 2));
    }
}
