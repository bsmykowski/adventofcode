package year2018.Day10;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import util.FilesReader;
import util.GlobalConstatns;

import java.util.ArrayList;
import java.util.List;

public class Day10 extends Application {

    private static final double SCALE = 1;
    private static final double WIDTH = 1000;
    private static final double HEIGHT = 1000;

    @Override
    public void start(Stage primaryStage) throws Exception {
        List<Point> points = getPoints();

        int time = 0;
        while(maxDist(points) > 100){
            animateOneFrame(points);
            time++;
        }
        System.out.println(time);

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(WIDTH * SCALE, HEIGHT * SCALE);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root, WIDTH * SCALE, HEIGHT * SCALE));
        primaryStage.show();
        AnimationTimer timer = new AnimationTimer() {

            private long lastUpdate;

            @Override
            public void start() {
                lastUpdate = System.nanoTime();
                super.start();
            }

            @Override
            public void handle(long now) {
                if(now - lastUpdate > 1000000000) {
                    drawCanvas(points, canvas);
                    animateOneFrame(points);
                    lastUpdate = now;
                }
            }
        };
        drawCanvas(points, canvas);
        timer.start();
    }

    private void animateOneFrame(List<Point> points) {
        for (Point point : points) {
            point.tick();
        }
    }

    private double maxDist(List<Point> points){
        double max = 0;
        for(Point point: points) {
            for(Point point2: points) {
                double distance = point.distance(point2);
                if(distance > max)
                    max = distance;
            }
        }
        return max;
    }

    private List<Point> getPoints() {
        String path = GlobalConstatns.getPath(2018, 10);
        List<String> lines = FilesReader.getLines(path);
        List<Point> points = new ArrayList<>();
        for (String line : lines) {
            points.add(new Point(line));
        }
        return points;
    }

    private void drawCanvas(List<Point> mainPoints, Canvas canvas) {
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        graphicsContext2D.setFill(Color.WHITE);
        graphicsContext2D.fillRect(0, 0, WIDTH*SCALE, HEIGHT*SCALE);
        for (Point point : mainPoints) {
            graphicsContext2D.setFill(Color.RED);
            graphicsContext2D.fillRect((point.getX() + WIDTH/2) * SCALE, (point.getY() + HEIGHT/2) * SCALE, SCALE, SCALE);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
