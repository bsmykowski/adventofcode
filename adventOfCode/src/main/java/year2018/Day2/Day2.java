package year2018.Day2;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day2 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2018, 2);
        List<String> lines = FilesReader.getLines(path);

        printResult(lines);

    }

    private static void printResult(List<String> lines) {
        for(int i = 0; i < lines.size() - 1; i++){
            for(int j = i + 1; j < lines.size(); j++) {
                String first = lines.get(i);
                String second = lines.get(j);
                if(areSimilarIDs(first, second)){
                    System.out.println(getWithRemovedDifference(first, second));
                }
            }
        }
    }

    private static int getChecksum(List<String> lines) {
        int doublesCounter = 0;
        int triplesCounter = 0;

        for(String line: lines) {
            Map<Character, Integer> chars = countChars(line);
            if (chars.containsValue(2)) {
                doublesCounter++;
            }
            if (chars.containsValue(3)) {
                triplesCounter++;
            }
        }

        return doublesCounter * triplesCounter;
    }

    private static Map<Character, Integer> countChars(String line){
        Map<Character, Integer> characters = new HashMap<>();
        for(int i = 0; i < line.length(); i++){
            char character = line.charAt(i);
            if(characters.containsKey(character)){
                int newCount = characters.get(character) + 1;
                characters.replace(character, newCount);
            } else {
                characters.put(character, 1);
            }
        }
        return characters;
    }

    private static boolean areSimilarIDs(String first, String second){
        if(first.length() != second.length()){
            return false;
        }
        int differenceCounter = 0;
        for(int i = 0; i < first.length(); i++) {
            if(first.charAt(i) != second.charAt(i)){
                differenceCounter++;
            }
        }
        if(differenceCounter == 1){
            return true;
        }
        return false;
    }

    private static String getWithRemovedDifference(String first, String second) {
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < first.length(); i++) {
            if(first.charAt(i) == second.charAt(i)){
                result.append(first.charAt(i));
            }
        }
        return result.toString();
    }

}
