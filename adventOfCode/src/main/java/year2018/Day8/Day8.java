package year2018.Day8;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.Arrays;
import java.util.List;

public class Day8 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2018, 8);
        List<String> lines = FilesReader.getLines(path);

        String[] tokens = lines.get(0).split(" ");
        for(String token:tokens){
            System.out.print(token+" ");
        }
        System.out.println();

        Node tree = new Node(Arrays.asList(tokens));

        tree.print();

        System.out.println(tree.nodeValue2());
    }

}
