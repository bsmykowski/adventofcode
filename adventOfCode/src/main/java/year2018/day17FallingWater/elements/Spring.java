package year2018.day17FallingWater.elements;

import javafx.scene.paint.Color;
import year2018.day17FallingWater.Point;

public class Spring extends Water {
    public Spring(Point position) {
        super(position, Color.AQUA);
    }
}
