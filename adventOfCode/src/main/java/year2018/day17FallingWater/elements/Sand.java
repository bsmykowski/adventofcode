package year2018.day17FallingWater.elements;

import javafx.scene.paint.Color;
import year2018.day17FallingWater.Point;

public class Sand extends ScanElement {
    public Sand(Point position){
        super(position, Color.LIGHTYELLOW);
    }

}
