package year2018.day17FallingWater.elements;

import javafx.scene.paint.Color;
import year2018.day17FallingWater.Point;

public class FallingWater extends Water {


    public FallingWater(Point position) {
        super(position, Color.BLUE);
    }
}
