package year2018.day17FallingWater.elements;

import javafx.scene.paint.Color;
import year2018.day17FallingWater.Point;

public abstract class Water extends ScanElement {
    public Water(Point position, Color color) {
        super(position, color);
    }
}
