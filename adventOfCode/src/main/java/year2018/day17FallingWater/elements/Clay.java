package year2018.day17FallingWater.elements;

import javafx.scene.paint.Color;
import year2018.day17FallingWater.Point;

public class Clay extends ScanElement {
    public Clay(Point position) {
        super(position, Color.SANDYBROWN);
    }
}
