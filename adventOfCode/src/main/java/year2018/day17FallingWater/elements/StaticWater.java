package year2018.day17FallingWater.elements;

import javafx.scene.paint.Color;
import year2018.day17FallingWater.Point;

public class StaticWater extends Water {
    public StaticWater(Point position) {
        super(position, Color.DARKBLUE);
    }
}
