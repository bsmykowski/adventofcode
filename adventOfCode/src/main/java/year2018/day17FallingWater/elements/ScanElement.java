package year2018.day17FallingWater.elements;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import year2018.day17FallingWater.Point;

@Data
@AllArgsConstructor
public abstract class ScanElement {
    private Point position;
    private Color color;

    public int getX(){
        return position.getX();
    }

    public int getY(){
        return position.getY();
    }
}
