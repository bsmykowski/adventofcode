package year2018.day17FallingWater;

import lombok.Data;
import year2018.day17FallingWater.elements.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Scan {
    private static final int SPRING_X = 500;
    private static final int SPRING_Y = 0;

    private List<List<ScanElement>> elements;
    private Map<Point, Water> water = new HashMap<>();
    private int width;
    private int height;
    private int minY;
    private int maxY;
    private boolean end = false;

    public Scan(List<String> dataLines){
        List<Line> lines = new ArrayList<>();
        for(String dataLine : dataLines){
            Line line = new Line(dataLine);
            lines.add(line);
        }

        minY = getMinY(lines);
        maxY = getMaxY(lines);

        width = getMaxX(lines) + 10;
        height = 2000;
        elements = new ArrayList<>();
        for(int j = 0; j < height; j++){
            List<ScanElement> row = new ArrayList<>();
            for(int i = 0; i < width; i++){
                row.add(new Sand(new Point(i, j)));
            }
            elements.add(row);
        }

        for(Line line : lines){
            line.getPoints().forEach(p-> setScanElement(new Clay(p)));
        }

        Point springPosition = new Point(SPRING_X, SPRING_Y);
        water.put(springPosition, new Spring(springPosition));
    }

    public void doStep(){
        if(end){
            return;
        }
        for(Water oneWater:water.values()){
            if(oneWater instanceof FallingWater && oneWater.getPosition().getNextTo(Direction.DOWN).getY() > maxY) {
                end = true;
                System.out.println(countWater());
                System.out.println(countStaticWater());
                return;
            }
        }
        List<Water> newWater = new ArrayList<>();
        for(Water oneWater:water.values()){
            if(oneWater instanceof FallingWater || oneWater instanceof Spring) {
                Point point = oneWater.getPosition();
                Point downPoint = oneWater.getPosition().getNextTo(Direction.DOWN);
                Point leftPoint = oneWater.getPosition().getNextTo(Direction.LEFT);
                Point rightPoint = oneWater.getPosition().getNextTo(Direction.RIGHT);
                if (isFree(downPoint)) {
                    newWater.add(new FallingWater(downPoint));
                } else if ((isStaticWater(downPoint) || isClay(downPoint))
                        && !isFallingWater(leftPoint)
                        && !isFallingWater(rightPoint)) {
                    newWater.addAll(getFilledLine(point));
                } else if (isStaticWater(downPoint)
                        && (isFree(leftPoint) || isFree(rightPoint))) {
                    newWater.addAll(getFilledLine(point));
                }
            }
        }
        newWater.forEach(w->water.put(w.getPosition(), w));
    }

    private List<Water> getFilledLine(Point downPoint) {
        List<Water> newWater = new ArrayList<>();
        List<Point> pointsToAdd = new ArrayList<>();
        Point left = downPoint;
        Point down = left.getNextTo(Direction.DOWN);
        while (!isClay(left) && !isFree(down)) {
            pointsToAdd.add(left);
            down = left.getNextTo(Direction.DOWN);
            left = left.getNextTo(Direction.LEFT);
        }
        down = downPoint.getNextTo(Direction.DOWN);
        Point right = downPoint.getNextTo(Direction.RIGHT);
        while (!isClay(right) && !isFree(down)){
            pointsToAdd.add(right);
            down = right.getNextTo(Direction.DOWN);
            right = right.getNextTo(Direction.RIGHT);
        }

        boolean staticWater = true;
        if(isFree(left.getNextTo(Direction.DOWN)) || isFree(right.getNextTo(Direction.DOWN))){
            staticWater = false;
        }

        for(Point point:pointsToAdd){
            if(staticWater)
                newWater.add(new StaticWater(point));
            else
                newWater.add(new FallingWater(point));
        }
        return newWater;
    }

    public int countWater(){
        int counter = 0;
        for(Water oneWater:water.values()){
            if(oneWater.getPosition().getY() <= maxY && oneWater.getPosition().getY() >= minY){
                counter++;
            }
        }
        return counter;
    }

    public int countStaticWater(){
        int counter = 0;
        for(Water oneWater:water.values()){
            if(oneWater instanceof StaticWater && oneWater.getPosition().getY() <= maxY && oneWater.getPosition().getY() >= minY){
                counter++;
            }
        }
        return counter;
    }

    private boolean isClay(Point downPoint) {
        return getScanElementAt(downPoint) instanceof Clay;
    }

    private boolean isStaticWater(Point point){
        Water pointWater = water.get(point);
        if(pointWater == null){
            return false;
        }
        return pointWater instanceof StaticWater;
    }

    private boolean isFallingWater(Point point){
        Water pointWater = water.get(point);
        if(pointWater == null){
            return false;
        }
        return pointWater instanceof FallingWater;
    }

    private boolean isFree(Point point){
        if(!(getScanElementAt(point) instanceof Sand)){
            return false;
        }
        Water pointWater = water.get(point);
        return pointWater == null;
    }

    private int getMaxX(List<Line> lines){
        int maxX = Integer.MIN_VALUE;
        for(Line line:lines){
            if(line.getMaxX() > maxX){
                maxX = line.getMaxX();
            }
        }
        return maxX;
    }

    private int getMaxY(List<Line> lines){
        int maxY = Integer.MIN_VALUE;
        for(Line line:lines){
            if(line.getMaxY() > maxY){
                maxY = line.getMaxY();
            }
        }
        return maxY;
    }

    private int getMinY(List<Line> lines){
        int minY = Integer.MAX_VALUE;
        for(Line line:lines){
            if(line.getMinY() < minY){
                minY = line.getMinY();
            }
        }
        return minY;
    }

    public ScanElement getScanElementAt(Point position){
        return elements.get(position.getY()).get(position.getX());
    }

    public void setScanElement(ScanElement scanElement){
        elements.get(scanElement.getPosition().getY()).set(scanElement.getPosition().getX(), scanElement);
    }
}
