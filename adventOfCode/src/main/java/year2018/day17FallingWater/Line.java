package year2018.day17FallingWater;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Line {
    private LineTypes type;
    private List<Point> points;

    public Line(String data){
        points = new ArrayList<>();
        String[] tokens = data.split(" ");
        int firstNumber = Integer.parseInt(tokens[0].substring(tokens[0].indexOf('=')+1, tokens[0].indexOf(',')));
        int rangeBegin = Integer.parseInt(tokens[1].substring(tokens[1].indexOf("=")+1, tokens[1].indexOf(".")));
        int rangeEnd = Integer.parseInt(tokens[1].substring(tokens[1].lastIndexOf(".")+1));
        if(data.charAt(0) == 'x'){
            type = LineTypes.HORIZONTAL;
            for(int i = rangeBegin; i <= rangeEnd; i++){
                points.add(new Point(firstNumber, i));
            }
        } else {
            type = LineTypes.VERTICAL;
            for(int i = rangeBegin; i <= rangeEnd; i++){
                points.add(new Point(i, firstNumber));
            }
        }

    }

    public int getMinY(){
        if(points.isEmpty()){
            return Integer.MIN_VALUE;
        }
        return points.get(0).getY();
    }

    public int getMaxY(){
        if(points.isEmpty()){
            return Integer.MAX_VALUE;
        }
        return points.get(points.size()-1).getY();
    }

    public int getMaxX(){
        if(points.isEmpty()){
            return Integer.MAX_VALUE;
        }
        return points.get(points.size()-1).getX();
    }

}
