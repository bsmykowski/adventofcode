package year2018.day17FallingWater;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Point {
    private int x;
    private int y;

    public boolean isTheSame(Point point){
        return point.x==x && point.y ==y;
    }

    public Point getNextTo(Direction direction){
        switch(direction){
            case LEFT:
                return new Point(x-1, y);
            case RIGHT:
                return new Point(x+1, y);
            case UP:
                return new Point(x, y-1);
            case DOWN:
                return new Point(x, y+1);
            default:
                return new Point(x, y);
        }
    }
}
