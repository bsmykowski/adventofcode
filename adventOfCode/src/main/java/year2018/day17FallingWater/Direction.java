package year2018.day17FallingWater;

public enum Direction {
    LEFT, RIGHT, UP, DOWN, NONE
}
