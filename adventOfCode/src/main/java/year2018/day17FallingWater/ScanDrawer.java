package year2018.day17FallingWater;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import year2018.day17FallingWater.elements.Clay;
import year2018.day17FallingWater.elements.ScanElement;
import year2018.day17FallingWater.elements.Water;

@Data
@AllArgsConstructor
public class ScanDrawer {
    private double height;
    private double width;
    private double scale;

    public void draw(Scan scan, double scale, GraphicsContext graphicsContext2D){

        graphicsContext2D.setFill(Color.LIGHTYELLOW);
        graphicsContext2D.fillRect(0, 0, width*scale, height*scale);

        for(int j = 0; j < scan.getHeight(); j++){
            for(int i = 0; i < scan.getWidth(); i++){
                ScanElement scanElement = scan.getScanElementAt(new Point(i,j));
                if(scanElement instanceof Clay)
                    drawFilledRect(scale, graphicsContext2D, scanElement);
            }
        }

        for(Water water: scan.getWater().values()){
            drawFilledRect(scale, graphicsContext2D, water);
        }

    }

    private void drawFilledRect(double scale, GraphicsContext graphicsContext2D, ScanElement mapElement) {
        graphicsContext2D.setFill(mapElement.getColor());
        graphicsContext2D.fillRect(mapElement.getX() * scale, mapElement.getY() * scale, scale, scale);
    }

    private void drawRect(double scale, GraphicsContext graphicsContext2D, ScanElement mapElement) {
        graphicsContext2D.setLineWidth(3.0);
        graphicsContext2D.setStroke(mapElement.getColor());
        graphicsContext2D.strokeRect(mapElement.getX() * scale, mapElement.getY() * scale, scale, scale);
    }

}
