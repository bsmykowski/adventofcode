package year2018.day17FallingWater;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import util.FilesReader;
import util.GlobalConstatns;

import java.util.List;

public class Day17 extends Application {
    private static final double SCALE = 3;
    private static final double WIDTH = 1000;
    private static final double HEIGHT = 2000;
    private static final double SCENE_HEIGHT = 1000;
    private static final double SCENE_WIDTH = 1000;

    @Override
    public void start(Stage primaryStage) throws Exception {

        String path = GlobalConstatns.getPath(2018, "day17FallingWater");
        List<String> lines = FilesReader.getLines(path);

        Scan scan = new Scan(lines);

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(WIDTH * SCALE, HEIGHT * SCALE);
        ScrollPane scrollPane = new ScrollPane(root);
        scrollPane.setHvalue(.5);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(scrollPane, SCENE_WIDTH, SCENE_HEIGHT));
        primaryStage.show();

        AnimationTimer timer = new AnimationTimer() {

            private long lastUpdate;

            @Override
            public void start() {
                lastUpdate = System.nanoTime();
                super.start();
            }

            @Override
            public void handle(long now) {
                double seconds = .01;
                if(now - lastUpdate > 1000000000*seconds) {
                    for(int i = 0; i < 1; i++)
                        scan.doStep();
                    lastUpdate = now;
                    drawCanvas(scan, canvas);
                }
            }
        };
        drawCanvas(scan, canvas);
        timer.start();
    }


    private void drawCanvas(Scan scan, Canvas canvas) {
        ScanDrawer mapDrawer = new ScanDrawer(HEIGHT, WIDTH, SCALE);
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        mapDrawer.draw(scan, SCALE, graphicsContext2D);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
