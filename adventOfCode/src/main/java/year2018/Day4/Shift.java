package year2018.Day4;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class Shift {
    private List<Event> events;
    private int guardId;
    private List<Integer> sleepMinutes;

    public Shift(Event startEvent){
        events = new ArrayList<>();
        events.add(startEvent);
        guardId = startEvent.getId();
        sleepMinutes = new ArrayList<>();
    }

    public void addEvent(Event event){
        events.add(event);
        event.setId(guardId);
    }

    public void updateSleepMinutes(){
        sleepMinutes.clear();
        int fallSleepMinute = 0;
        for(Event event : events){
            if(event.getType() == EventType.FALL_ASLEEP){
                fallSleepMinute = event.getDate().getMinute();
            } else if(event.getType() == EventType.WAKE_UP){
                int wakeUpMinute = event.getDate().getMinute();
                for(int i = fallSleepMinute; i < wakeUpMinute; i++){
                    sleepMinutes.add(i);
                }
            }
        }
    }

    public int getSleepTime(){
        updateSleepMinutes();
        return sleepMinutes.size();
    }

}
