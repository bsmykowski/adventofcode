package year2018.Day4;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Guard {

    private List<Shift> shifts;
    private int id;

    public Guard(){
        shifts = new ArrayList<>();
    }

    public void addShift(Shift shift){
        shifts.add(shift);
        id = shift.getGuardId();
    }

    public int getAllShiftsSleepTime(){
        int overallSleepTime = 0;
        for(Shift shift:shifts){
            overallSleepTime += shift.getSleepTime();
        }
        return overallSleepTime;
    }

    public int getMostFrequentSleepMinute(){
        Map<Integer, Integer> sleepMinutesCount = getMinutesSleepQuantityMap();
        int max = 0;
        int maxMinute = 0;
        for(Map.Entry<Integer, Integer> entry : sleepMinutesCount.entrySet()){
            if(entry.getValue() > max){
                max = entry.getValue();
                maxMinute = entry.getKey();
            }
        }
        return maxMinute;
    }

    public Map<Integer, Integer> getMinutesSleepQuantityMap() {
        Map<Integer, Integer> sleepMinutesCount = new HashMap<>();
        for(Shift shift:shifts){
            shift.updateSleepMinutes();
            for(Integer minute : shift.getSleepMinutes()){
                if(sleepMinutesCount.containsKey(minute)){
                    sleepMinutesCount.replace(minute, sleepMinutesCount.get(minute) + 1);
                } else {
                    sleepMinutesCount.put(minute, 1);
                }
            }
        }
        return sleepMinutesCount;
    }

}
