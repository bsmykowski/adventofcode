package year2018.Day4;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.*;

public class Day4 {

    public static void main(String[] args) {

        String path = GlobalConstatns.getPath(2018, 4);
        List<String> lines = FilesReader.getLines(path);

        List<Event> events = new ArrayList<>();
        Map<Integer, Guard> guards = new HashMap<>();

        for (String line : lines){
            Event event = new Event(line);
            events.add(event);
        }

        events.sort(Comparator.comparing(Event::getDate));

        Shift shift = new Shift();
        for(Event event:events){
            if(event.getType() == EventType.START){
                shift = new Shift(event);
                Guard guard = guards.getOrDefault(event.getId(), new Guard());
                guard.addShift(shift);
                guards.putIfAbsent(guard.getId(), guard);
            } else {
                shift.addEvent(event);
            }
        }

        strategy2(guards);
    }

    private static void strategy2(Map<Integer, Guard> guards) {
        int maxSleepTimes = 0;
        int maxSleepTimeId = -1;
        int maxSleepMinute = 0;
        for(Guard guard:guards.values()){
            for(Map.Entry<Integer, Integer> entry :guard.getMinutesSleepQuantityMap().entrySet()){
                if(entry.getValue() > maxSleepTimes){
                    maxSleepTimes = entry.getValue();
                    maxSleepMinute = entry.getKey();
                    maxSleepTimeId = guard.getId();
                }
            }
        }

        Guard guard = guards.get(maxSleepTimeId);
        System.out.println(guard.getId() * maxSleepMinute);
    }

    private static void strategy1(Map<Integer, Guard> guards) {
        int maxSleepTime = 0;
        int maxSleepTimeId = -1;
        for(Guard guard:guards.values()){
            int allShiftsSleepTime = guard.getAllShiftsSleepTime();
            if(allShiftsSleepTime > maxSleepTime){
                maxSleepTime = allShiftsSleepTime;
                maxSleepTimeId = guard.getId();
            }
        }

        Guard guard = guards.get(maxSleepTimeId);
        System.out.println(guard.getId() + " " + guard.getAllShiftsSleepTime() + " " + guard.getMostFrequentSleepMinute());
        System.out.println(guard.getId() * guard.getMostFrequentSleepMinute());
    }

}
