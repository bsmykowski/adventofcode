package year2018.Day4;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class Event {
    private static DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    private LocalDateTime date;
    private EventType type;
    private int id;

    public Event(String line){
        String[] tokens = line.split("]");

        String timeString = tokens[0].substring(1);
        date = getDate(timeString);

        type = getEventTypeAndAssignId(tokens);
    }

    protected EventType getEventTypeAndAssignId(String[] tokens){
        EventType type = EventType.WAKE_UP;
        if(tokens[1].charAt(1) == 'G'){
            String[] commandTokens = tokens[1].split(" ");
            id = Integer.parseInt(commandTokens[2].substring(1));
            type = EventType.START;
        } else if(tokens[1].charAt(1) == 'f'){
            type = EventType.FALL_ASLEEP;
        }
        return type;
    }

    private LocalDateTime getDate(String time) {
        return LocalDateTime.parse(time, format);
    }

    public void print(){
        System.out.println(date + " " + type + " " + id);
    }
}
