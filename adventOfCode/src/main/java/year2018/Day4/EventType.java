package year2018.Day4;

public enum EventType {
    START, FALL_ASLEEP, WAKE_UP
}
