package year2018.Day5;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day5 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2018, 5);
        List<String> lines = FilesReader.getLines(path);

        StringBuilder polymer = new StringBuilder(lines.get(0));

        int minLength = Integer.MAX_VALUE;
        long start = System.currentTimeMillis();

        Set<Character> allCharacters = getAllCharacters(polymer);
        for(Character character:allCharacters){
            System.out.println(character);
            StringBuilder newPolymer = new StringBuilder(polymer);
            removeAllCharacters(newPolymer, character);
            System.out.println(System.currentTimeMillis() - start);
            performReaction(newPolymer);
            System.out.println(System.currentTimeMillis() - start);
            int length = newPolymer.length();
            if(length < minLength){
                minLength = length;
            }
        }
        System.out.println(minLength);

    }

    private static void removeAllCharacters(StringBuilder polymer, Character character){
        char upper = Character.toUpperCase(character);
        for(int i = polymer.length() - 1; i >= 0; i--){
            char charAt = polymer.charAt(i);
            if(charAt == character || charAt == upper){
                polymer.delete(i, i+1);
            }
        }
    }

    private static Set<Character> getAllCharacters(StringBuilder polymer){
        Set<Character> allCharacters = new HashSet<>();
        for(Character character:polymer.toString().toCharArray()){
            allCharacters.add(Character.toLowerCase(character));
        }
        return allCharacters;
    }

    private static void task1(StringBuilder polymer) {
        performReaction(polymer);
        System.out.println(polymer);
        System.out.println(polymer.length());
    }

    private static void performReaction(StringBuilder polymer) {
        List<Integer> indicesOfFirstPairs = getIndicesOfFirstPairs(polymer);
        while(!indicesOfFirstPairs.isEmpty()){
            for(Integer indexOfFirstPair : indicesOfFirstPairs){
                polymer.delete(indexOfFirstPair, indexOfFirstPair+2);
            }
            indicesOfFirstPairs = getIndicesOfFirstPairs(polymer);
        }
    }

    private static List<Integer> getIndicesOfFirstPairs(StringBuilder polymer){
        List<Integer> indicesOfPairs = new ArrayList<>();
        for(int i = polymer.length() - 2; i >= 0; i--){
            char first = polymer.charAt(i);
            char second = polymer.charAt(i+1);
            if(arePair(first, second)){
                if(indicesOfPairs.isEmpty() || indicesOfPairs.get(indicesOfPairs.size() - 1) - i > 1)
                    indicesOfPairs.add(i);
            }
        }
        return indicesOfPairs;
    }

    private static boolean arePair(char first, char second){
        if(Character.toLowerCase(first) != Character.toLowerCase(second)){
            return false;
        }
        if(Character.isUpperCase(first) && Character.isUpperCase(second)){
            return false;
        }
        if(Character.isLowerCase(first) && Character.isLowerCase(second)){
            return false;
        }
        return true;
    }

}
