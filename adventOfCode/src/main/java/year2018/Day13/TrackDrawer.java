package year2018.Day13;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import year2018.Day13.model.TrackElement;
import year2018.Day13.model.TrackType;

public class TrackDrawer {
    private GraphicsContext graphicsContext2D;
    private double scale;

    public TrackDrawer(GraphicsContext graphicsContext2D,
                       double scale){
        this.graphicsContext2D = graphicsContext2D;
        this.scale = scale;
    }

    public void drawTrackElement(TrackElement trackElement){
        Color pointColor = Color.RED;
        TrackType type = trackElement.getType();
        double lineWidth = scale/3;
        if(TrackType.HORIZONTAL.equals(type)){
            graphicsContext2D.setStroke(pointColor);
            graphicsContext2D.setLineWidth(lineWidth);
            graphicsContext2D.strokeLine(trackElement.getX() * scale,
                                        trackElement.getY() * scale + scale/2,
                                        trackElement.getX() * scale + scale,
                                        trackElement.getY() * scale + scale/2);
        } else if(TrackType.VERTICAL.equals(type)){
            graphicsContext2D.setStroke(pointColor);
            graphicsContext2D.setLineWidth(lineWidth);
            graphicsContext2D.strokeLine(trackElement.getX() * scale + scale/2,
                                        trackElement.getY() * scale,
                                        trackElement.getX() * scale + scale/2,
                                        trackElement.getY() * scale + scale);
        } else if(TrackType.CROSS.equals(type)){
            graphicsContext2D.setStroke(pointColor);
            graphicsContext2D.setLineWidth(lineWidth);
            graphicsContext2D.strokeLine(trackElement.getX() * scale + scale/2,
                    trackElement.getY() * scale,
                    trackElement.getX() * scale + scale/2,
                    trackElement.getY() * scale + scale);
            graphicsContext2D.strokeLine(trackElement.getX() * scale,
                    trackElement.getY() * scale + scale/2,
                    trackElement.getX() * scale + scale,
                    trackElement.getY() * scale + scale/2);
        } else if(TrackType.LEFT_TOP.equals(type)){
            graphicsContext2D.setStroke(pointColor);
            graphicsContext2D.setLineWidth(lineWidth);
            graphicsContext2D.strokeLine(trackElement.getX() * scale + scale/2,
                    trackElement.getY() * scale + scale,
                    trackElement.getX() * scale + scale,
                    trackElement.getY() * scale + scale/2);
        } else if(TrackType.LEFT_DOWN.equals(type)){
            graphicsContext2D.setStroke(pointColor);
            graphicsContext2D.setLineWidth(lineWidth);
            graphicsContext2D.strokeLine(trackElement.getX() * scale + scale/2,
                    trackElement.getY() * scale,
                    trackElement.getX() * scale + scale,
                    trackElement.getY() * scale + scale/2);
        } else if(TrackType.RIGHT_TOP.equals(type)){
            graphicsContext2D.setStroke(pointColor);
            graphicsContext2D.setLineWidth(lineWidth);
            graphicsContext2D.strokeLine(trackElement.getX() * scale,
                    trackElement.getY() * scale + scale/2,
                    trackElement.getX() * scale + scale/2,
                    trackElement.getY() * scale + scale);
        } else if(TrackType.RIGHT_DOWN.equals(type)){
            graphicsContext2D.setStroke(pointColor);
            graphicsContext2D.setLineWidth(lineWidth);
            graphicsContext2D.strokeLine(trackElement.getX() * scale,
                    trackElement.getY() * scale + scale/2,
                    trackElement.getX() * scale + scale/2,
                    trackElement.getY() * scale);
        } else if(TrackType.NONE.equals(type)){
            graphicsContext2D.setFill(Color.WHITE);
            graphicsContext2D.fillRect(trackElement.getX() * scale, trackElement.getY() * scale, scale, scale);
        } else if(TrackType.CART.equals(type)){
            graphicsContext2D.setFill(Color.BLACK);
            graphicsContext2D.fillRect(trackElement.getX() * scale, trackElement.getY() * scale, scale, scale);
        }
    }

}
