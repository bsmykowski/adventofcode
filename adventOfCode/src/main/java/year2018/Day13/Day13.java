package year2018.Day13;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import util.FilesReader;
import util.GlobalConstatns;
import year2018.Day13.model.Cart;
import year2018.Day13.model.TrackMap;
import year2018.Day13.model.TrackElement;

import java.util.List;

public class Day13 extends Application {
    private static final double SCALE = 6;
    private static final double WIDTH = 150;
    private static final double HEIGHT = 150;

    @Override
    public void start(Stage primaryStage) throws Exception {
        String path = GlobalConstatns.getPath(2018, 13);
        List<String> lines = FilesReader.getLines(path);

        TrackMap map = new TrackMap(lines);

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(WIDTH * SCALE, HEIGHT * SCALE);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root, WIDTH * SCALE, HEIGHT * SCALE));
        primaryStage.show();
        AnimationTimer timer = new AnimationTimer() {

            private long lastUpdate;

            @Override
            public void start() {
                lastUpdate = System.nanoTime();
                super.start();
            }

            @Override
            public void handle(long now) {
                double seconds = 0.1;
                if(now - lastUpdate > 1000000000*seconds) {
                    drawCanvas(map, canvas);
                    for(int i = 0; i < 1; i++) {
                        map.makeStep();
                    }
                    lastUpdate = now;
                }
            }
        };
        drawCanvas(map, canvas);
        timer.start();
    }

    private void drawCanvas(TrackMap map, Canvas canvas) {
        List<List<TrackElement>> points = map.getPoints();
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        graphicsContext2D.setFill(Color.WHITE);
        graphicsContext2D.fillRect(0, 0, WIDTH*SCALE, HEIGHT*SCALE);
        TrackDrawer pointDrawer = new TrackDrawer(graphicsContext2D, SCALE);
        for (List<TrackElement> row : points) {
            for (TrackElement point : row) {
                pointDrawer.drawTrackElement(point);
            }
        }
        CartDrawer cartDrawer = new CartDrawer(graphicsContext2D, SCALE);
        for (Cart cart : map.getCarts()){
            cartDrawer.drawCart(cart);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
