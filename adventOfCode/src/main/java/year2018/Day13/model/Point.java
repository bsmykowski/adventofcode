package year2018.Day13.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Point {
   private int x;
   private int y;

   public void move(Direction direction){
      Point next = getNext(direction);
      x = next.x;
      y = next.y;
   }

   public Point getNext(Direction direction){
      Point next = new Point(x, y);
      switch(direction){
         case LEFT:
            next.x--;
            break;
         case RIGHT:
            next.x++;
            break;
         case UP:
            next.y--;
            break;
         case DOWN:
            next.y++;
            break;
      }
      return next;
   }
}
