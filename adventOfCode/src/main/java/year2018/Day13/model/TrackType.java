package year2018.Day13.model;

public enum TrackType {
    HORIZONTAL, VERTICAL, CROSS, LEFT_TOP, RIGHT_TOP, LEFT_DOWN, RIGHT_DOWN, NONE, CART;

    public static TrackType getPointType(char character, TrackType leftNeighbourType){
        if(character == '-'){
            return HORIZONTAL;
        } else if(character == '|'){
            return VERTICAL;
        } else if(character == '+'){
            return CROSS;
        } else if(character == '/'){
            if(leftNeighbourType == NONE
                    || leftNeighbourType == VERTICAL
                    || leftNeighbourType == RIGHT_DOWN
                    || leftNeighbourType == RIGHT_TOP){
                return LEFT_TOP;
            } else {
                return RIGHT_DOWN;
            }
        } else if(character == '\\') {
            if (leftNeighbourType == NONE
                    || leftNeighbourType == VERTICAL
                    || leftNeighbourType == RIGHT_DOWN
                    || leftNeighbourType == RIGHT_TOP) {
                return LEFT_DOWN;
            } else {
                return RIGHT_TOP;
            }
        } else if(character != ' '){
            if (leftNeighbourType == HORIZONTAL
                    || leftNeighbourType == LEFT_TOP
                    || leftNeighbourType == LEFT_DOWN
                    || leftNeighbourType == CROSS) {
                return HORIZONTAL;
            } else {
                return VERTICAL;
            }
        }
        return NONE;
    }
}
