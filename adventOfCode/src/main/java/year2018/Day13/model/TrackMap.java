package year2018.Day13.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TrackMap {
    private List<List<TrackElement>> points;
    private List<Cart> carts;

    public TrackMap(List<String> lines){
        points = new ArrayList<>();
        for(int j = 0; j < lines.size(); j++){
            List<TrackElement> row = new ArrayList<>();
            String line = lines.get(j);
            TrackType leftNeighbourType = TrackType.NONE;
            for(int i = 0; i < line.length(); i++){
                TrackElement newPoint = new TrackElement(i, j, line.charAt(i), leftNeighbourType);
                row.add(newPoint);
                leftNeighbourType = newPoint.getType();
            }
            points.add(row);
        }
        carts = new ArrayList<>();
        for(int j = 0; j < lines.size(); j++){
            String line = lines.get(j);
            for(int i = 0; i < line.length(); i++){
                char character = line.charAt(i);
                if(character == '<'
                        || character == '>'
                        || character == 'v'
                        || character == '^') {
                    carts.add(new Cart(character, i, j));
                }
            }
        }
    }

    public void makeStep(){
        List<Cart> cartsToRemove = new ArrayList<>();
        carts.sort((o1, o2) -> {
            if (o1.getY() == o2.getY())
                return Double.compare(o1.getX(), o2.getX());
            return Double.compare(o1.getY(), o2.getY());
        });
        for(Cart cart: carts){
            Point nextPosition = cart.getNextPosition();
            TrackType nextTrackElement = getTrackTypeAt(nextPosition);
            cart.move(nextTrackElement);
            for(Cart cart2: carts) {
                if(cart2 != cart){
                    if(cart.collides(cart2)){
                        cartsToRemove.add(cart);
                        cartsToRemove.add(cart2);
                        System.out.println(cart.getX()+","+cart.getY());
                    }
                }
            }
        }
        carts.removeAll(cartsToRemove);
        if(carts.size() == 1){
            Cart cart = carts.get(0);
            System.out.println("last: " + cart.getX()+","+cart.getY());
        }
    }

    public TrackType getTrackTypeAt(Point point){
        if(point.getY() < points.size() && point.getY() >= 0
                && point.getX() < points.get(point.getY()).size() && point.getX() >= 0)
            return points.get(point.getY()).get(point.getX()).getType();
        return TrackType.NONE;
    }


}
