package year2018.Day13.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TrackElement {
    private Point position;
    private TrackType type;

    public TrackElement(int x, int y, char character, TrackType leftType){
        this(x,y);
        this.type = TrackType.getPointType(character, leftType);
    }

    public TrackElement(int x, int y){
        this.position = new Point(x, y);
    }

    public int getX(){
        return position.getX();
    }

    public int getY(){
        return position.getY();
    }

}
