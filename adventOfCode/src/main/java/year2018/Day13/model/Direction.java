package year2018.Day13.model;

public enum Direction {
    LEFT, RIGHT, UP, DOWN, NONE;

    public static Direction getLeftTo(Direction direction){
        switch(direction){
            case LEFT:
                return DOWN;
            case RIGHT:
                return UP;
            case UP:
                return LEFT;
            case DOWN:
                return RIGHT;
        }
        return NONE;
    }

    public static Direction getRightTo(Direction direction){
        switch(direction){
            case LEFT:
                return UP;
            case RIGHT:
                return DOWN;
            case UP:
                return RIGHT;
            case DOWN:
                return LEFT;
        }
        return NONE;
    }

}
