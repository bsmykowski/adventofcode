package year2018.Day13.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Cart {
    private static List<Direction> turnsSequence = new ArrayList<>();
    static {
        turnsSequence.add(Direction.LEFT);
        turnsSequence.add(Direction.NONE);
        turnsSequence.add(Direction.RIGHT);
    }

    private Point position;
    private Direction direction;
    private int counter;

    public Cart(char character, int x, int y){
        counter = 0;
        position = new Point(x, y);
        switch(character){
            case 'v':
                direction = Direction.DOWN;
                break;
            case '^':
                direction = Direction.UP;
                break;
            case '<':
                direction = Direction.LEFT;
                break;
            case '>':
                direction = Direction.RIGHT;
                break;
        }
    }

    public void move(TrackType nextTrackType){
        position.move(direction);
        if (nextTrackType.equals(TrackType.RIGHT_DOWN) && direction.equals(Direction.DOWN)){
            direction = Direction.LEFT;
        } else if (nextTrackType.equals(TrackType.RIGHT_DOWN) && direction.equals(Direction.RIGHT)){
            direction = Direction.UP;
        } else if (nextTrackType.equals(TrackType.LEFT_DOWN) && direction.equals(Direction.DOWN)){
            direction = Direction.RIGHT;
        } else if (nextTrackType.equals(TrackType.LEFT_DOWN) && direction.equals(Direction.LEFT)){
            direction = Direction.UP;
        } else if (nextTrackType.equals(TrackType.LEFT_TOP) && direction.equals(Direction.UP)){
            direction = Direction.RIGHT;
        } else if (nextTrackType.equals(TrackType.LEFT_TOP) && direction.equals(Direction.LEFT)){
            direction = Direction.DOWN;
        } else if (nextTrackType.equals(TrackType.RIGHT_TOP) && direction.equals(Direction.UP)){
            direction = Direction.LEFT;
        } else if (nextTrackType.equals(TrackType.RIGHT_TOP) && direction.equals(Direction.RIGHT)){
            direction = Direction.DOWN;
        } else if (nextTrackType.equals(TrackType.CROSS)){
            Direction turn = turnsSequence.get(counter%turnsSequence.size());
            if(turn.equals(Direction.RIGHT)){
                direction = Direction.getRightTo(direction);
            } else if(turn.equals(Direction.LEFT)){
                direction = Direction.getLeftTo(direction);
            }
            counter++;
        }
    }

    public boolean collides(Cart cart){
        return cart.getY() == getY() && cart.getX() == getX();
    }

    public double getX() {
        return position.getX();
    }

    public double getY() {
        return position.getY();
    }

    public Point getNextPosition() {
        return position.getNext(direction);
    }
}
