package year2018.Day13;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import year2018.Day13.model.Cart;

public class CartDrawer {

    private GraphicsContext graphicsContext2D;
    private double scale;

    public CartDrawer(GraphicsContext graphicsContext2D,
                       double scale){
        this.graphicsContext2D = graphicsContext2D;
        this.scale = scale;
    }

    public void drawCart(Cart cart){
        Color color = Color.BLUE;
        double lineWidth = scale/2;
        graphicsContext2D.setStroke(color);
        graphicsContext2D.setLineWidth(lineWidth);

        switch(cart.getDirection()){
            case LEFT:
                graphicsContext2D.strokeLine(cart.getX() * scale,
                        cart.getY() * scale + scale/2,
                        cart.getX() * scale + scale,
                        cart.getY() * scale);
                graphicsContext2D.strokeLine(cart.getX() * scale,
                        cart.getY() * scale + scale/2,
                        cart.getX() * scale + scale,
                        cart.getY() * scale + scale);
                break;
            case RIGHT:
                graphicsContext2D.strokeLine(cart.getX() * scale + scale,
                        cart.getY() * scale + scale/2,
                        cart.getX() * scale,
                        cart.getY() * scale);
                graphicsContext2D.strokeLine(cart.getX() * scale + scale,
                        cart.getY() * scale + scale/2,
                        cart.getX() * scale,
                        cart.getY() * scale + scale);
                break;
            case UP:
                graphicsContext2D.strokeLine(cart.getX() * scale + scale/2,
                        cart.getY() * scale,
                        cart.getX() * scale + scale,
                        cart.getY() * scale + scale);
                graphicsContext2D.strokeLine(cart.getX() * scale + scale/2,
                        cart.getY() * scale,
                        cart.getX() * scale,
                        cart.getY() * scale + scale);
                break;
            case DOWN:
                graphicsContext2D.strokeLine(cart.getX() * scale + scale/2,
                        cart.getY() * scale + scale,
                        cart.getX() * scale + scale,
                        cart.getY() * scale);
                graphicsContext2D.strokeLine(cart.getX() * scale + scale/2,
                        cart.getY() * scale + scale,
                        cart.getX() * scale,
                        cart.getY() * scale);
                break;
        }
    }

}
