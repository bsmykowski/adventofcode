package year2018.Day11;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Random;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Point {
    private static final int SERIAL_NUMBER = 9110;
    private static double maxTotalValue = 0;
    private static double maxAnySizeTotalValue = 0;
    private static Random random = new Random();
    private int x = 0;
    private int y = 0;
    private Color color;
    private int totalPower;
    private int totalPowerAnySize;
    private int bestSize;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
        color = getColorByValue();
    }

    private Color getColorByValue() {
        return Color.color(0.0, 0.0, (double)(getValue()+5)/10);
    }

    private Color getColorByTotalPower() {
        if(totalPower > maxTotalValue){
            maxTotalValue = totalPower;
            System.out.println(x+","+y + " " + totalPower);
        }
        double colorPart = (double) (totalPower + 45) / 81;
        return Color.color(colorPart, colorPart, colorPart);
    }

    private Color getColorByTotalPowerAndSize() {
        if(totalPowerAnySize > maxAnySizeTotalValue){
            maxAnySizeTotalValue = totalPowerAnySize;
            System.out.println(x+","+y+"," +bestSize + " " + totalPowerAnySize);
        }
        double colorPart = (double) (totalPowerAnySize + 5*bestSize*bestSize) / (9*bestSize*bestSize);
        return Color.color(0, 0, colorPart);
    }

    public void setTotalPower(int totalPower){
        this.totalPower = totalPower;
        color = getColorByTotalPower();
    }

    public void setTotalPowerAnySize(int totalPower, int size){
        this.totalPowerAnySize = totalPower;
        bestSize = size;
        color = getColorByTotalPowerAndSize();
    }

    public double distance(Point point){
        return Math.sqrt(Math.pow(point.x - x, 2) + Math.pow(point.y - y, 2));
    }

    public int getValue(){
        int rackId = x+10;
        return (rackId*y+SERIAL_NUMBER)*rackId/100%10-5;
    }
}
