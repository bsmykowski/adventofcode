package year2018.Day11;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Day11 extends Application {

    private static final double SCALE = 3;
    private static final double WIDTH = 300;
    private static final double HEIGHT = 300;
    private static final int SIZE = 300;

    @Override
    public void start(Stage primaryStage) throws Exception {
        List<List<Point>> points = getPoints();

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(WIDTH * SCALE, HEIGHT * SCALE);
        root.getChildren().add(canvas);
        drawCanvas(points, canvas);
        primaryStage.setScene(new Scene(root, WIDTH * SCALE, HEIGHT * SCALE));
        primaryStage.show();

    }

    private List<List<Point>> getPoints() {
        List<List<Point>> points = new ArrayList<>();
        for(int j = 0; j < SIZE; j++){
            List<Point> row = new ArrayList<>();
            for (int i = 0; i < SIZE; i++){
                row.add(new Point(j, i));
            }
            points.add(row);
        }
        task2(points);
        return points;
    }

    private void task2(List<List<Point>> points) {
        for(int i = 0; i < SIZE; i++){
            System.out.println(i+" ");
            for (int j = 0; j < SIZE; j++){
                int maxAnySize = 0;
                int bestSize = SIZE;
                for(int d = 0; d < SIZE-Math.max(i, j); d++) {
                    int totalValue = squareValue(points, i, j, d);
                    if (totalValue > maxAnySize){
                        maxAnySize = totalValue;
                        bestSize = d;
                    }
                }
                points.get(j).get(i).setTotalPowerAnySize(maxAnySize, bestSize);
            }
        }
    }

    private int squareValue(List<List<Point>> points, int i, int j, int d) {
        int totalValue = 0;
        for (int p = 0; p < d; p++) {
            for (int k = 0; k < d; k++) {
                totalValue += points.get(j + p).get(i + k).getValue();
            }
        }
        return totalValue;
    }

    private void drawCanvas(List<List<Point>> mainPoints, Canvas canvas) {
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        graphicsContext2D.setFill(Color.WHITE);
        graphicsContext2D.fillRect(0, 0, WIDTH*SCALE, HEIGHT*SCALE);
        for (List<Point> row : mainPoints) {
            for (Point point : row) {
                graphicsContext2D.setFill(point.getColor());
                graphicsContext2D.fillRect(point.getX() * SCALE, point.getY() * SCALE, SCALE, SCALE);
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

}
