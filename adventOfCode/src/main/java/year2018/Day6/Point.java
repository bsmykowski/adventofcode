package year2018.Day6;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Point {
    private int x = 0;
    private int y = 0;
    private Color color;
    private int mainPointId;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getDistance(Point point){
        return Math.abs(point.x - x) + Math.abs(point.y - y);
    }
}
