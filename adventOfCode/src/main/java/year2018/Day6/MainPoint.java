package year2018.Day6;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Random;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MainPoint extends Point {
    private static int ID_COUNTER = 0;
    private static Random random = new Random();

    private int id;


    public MainPoint(String line){
        String[] tokens = line.split(",");
        setX(Integer.parseInt(tokens[0]));
        setY(Integer.parseInt(tokens[1].substring(1)));
        id = generateId();
        setColor(getRandomColor());
    }

    private Color getRandomColor() {
        return Color.color(0.0, random.nextDouble(), random.nextDouble());
    }

    private static int generateId(){
        return ID_COUNTER++;
    }
}
