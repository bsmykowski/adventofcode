package year2018.Day6;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import util.FilesReader;
import util.GlobalConstatns;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day6 extends Application {
    private static final int SCALE = 2;
    private static final int WIDTH = 400;
    private static final int HEIGHT = 400;
    private static final int ALLOWED_DISTANCE = 10000;

    @Override
    public void start(Stage primaryStage) throws Exception{
        List<MainPoint> points = getMainPoints();

        List<List<Point>> allPoints = computeAllPoint(points);

        Map<Integer, Area> areas = getAreas(allPoints, points);

        checkInfinity(allPoints, areas);

        int maxSize = 0;
        for(Map.Entry<Integer, Area> entry : areas.entrySet()){
            int size = entry.getValue().getSize();
            System.out.println(entry.getKey() + " " + size + " " + entry.getValue().isInfinite());
            if(size > maxSize && !entry.getValue().isInfinite()){
                maxSize = size;
            }
        }
        System.out.println(maxSize);

        markSafePoints(allPoints, points);

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(WIDTH*SCALE, HEIGHT*SCALE);
        drawCanvas(allPoints, points, canvas);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root, WIDTH*SCALE, HEIGHT*SCALE));
        primaryStage.show();

    }

    private void markSafePoints(List<List<Point>> allPoints, List<MainPoint> points){
        int numberOfSafePoints = 0;
        for(List<Point> row : allPoints) {
            for (Point point : row) {
                if(isSafePoint(point, points)){
                    numberOfSafePoints++;
                    point.setColor(Color.BLACK);
                }
            }
        }
        System.out.println(numberOfSafePoints);
    }

    private boolean isSafePoint(Point point, List<MainPoint> points){
        int distanceSum = 0;
        for(MainPoint mainPoint: points){
            distanceSum +=point.getDistance(mainPoint);
        }
        if(distanceSum < ALLOWED_DISTANCE){
            return true;
        }
        return false;
    }

    private Map<Integer, Area> getAreas(List<List<Point>> allPoints, List<MainPoint> points) {
        Map<Integer, Area> areas = new HashMap<>();
        for(MainPoint mainPoint:points){
            Area area = new Area(mainPoint);
            for(List<Point> row : allPoints) {
                for (Point point : row) {
                    if (point.getMainPointId() == mainPoint.getId()) {
                        area.addPoint(point);
                    }
                }
            }
            areas.put(mainPoint.getId(), area);
        }
        return areas;
    }

    private void checkInfinity(List<List<Point>> allPoints, Map<Integer, Area> areas){
        for(List<Point> row : allPoints) {
            for (Point point : row) {
                int i = point.getX();
                int j = point.getY();
                if(i == 0 || j == 0 || i == WIDTH - 1 ||j == HEIGHT - 1){
                    if(point.getMainPointId() != -1) {
                        areas.get(point.getMainPointId()).setInfinite(true);
                    }
                }
            }
        }
    }

    private List<List<Point>> computeAllPoint(List<MainPoint> points){
        List<List<Point>> allPoints = new ArrayList<>();
        for(int i = 0; i < WIDTH; i++){
            List<Point> row = new ArrayList<>();
            for (int j = 0; j < HEIGHT; j++){
                Point newPoint = new Point(i, j);
                int minDistance = Integer.MAX_VALUE;
                for(MainPoint mainPoint : points){
                    int distance = mainPoint.getDistance(newPoint);
                    if(distance < minDistance){
                        minDistance = distance;
                        newPoint.setColor(mainPoint.getColor());
                        newPoint.setMainPointId(mainPoint.getId());
                    } else if(distance == minDistance){
                        newPoint.setColor(Color.WHITE);
                        newPoint.setMainPointId(-1);
                    }
                }
                row.add(newPoint);
            }
            allPoints.add(row);
        }
        return allPoints;
    }

    private List<MainPoint> getMainPoints() {
        String path = GlobalConstatns.getPath(2018, 6);
        List<String> lines = FilesReader.getLines(path);
        List<MainPoint> points = new ArrayList<>();
        for(String line:lines){
            points.add(new MainPoint(line));
        }
        return points;
    }

    private void drawCanvas(List<List<Point>> points, List<MainPoint> mainPoints, Canvas canvas) {
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        for(List<Point> row : points){
            for(Point point: row){
                graphicsContext2D.setFill(point.getColor());
                graphicsContext2D.fillRect(point.getX()*SCALE, point.getY()*SCALE, SCALE, SCALE);
            }
        }
        for(MainPoint point: mainPoints){
            graphicsContext2D.setFill(Color.RED);
            graphicsContext2D.fillRect(point.getX()*SCALE, point.getY()*SCALE, SCALE, SCALE);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
