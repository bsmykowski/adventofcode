package year2018.Day6;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Area {
    private MainPoint mainPoint;
    private List<Point> pointsInArea;
    private boolean infinite;

    public Area(MainPoint mainPoint){
        this.mainPoint = mainPoint;
        pointsInArea = new ArrayList<>();
        infinite = false;
    }

    public void addPoint(Point point) {
        pointsInArea.add(point);
    }

    public int getSize(){
        return pointsInArea.size();
    }
}
