package year2018.Day15.Map;

import lombok.Data;
import year2018.Day15.Map.Elements.Creatures.Creature;
import year2018.Day15.Map.Elements.Highlight;
import year2018.Day15.Map.Elements.MapElement;
import year2018.Day15.Map.Elements.Wall;
import year2018.Day15.Point;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Map;

@Data
public class GameMap {
    private List<List<MapElement>> map;
    private int width;
    private int height;
    private List<Creature> creatures;
    private int currentStepCreature;
    private List<Highlight> highlights;
    private int round;
    private boolean endGame;

    public GameMap(List<String> lines){
        endGame = false;
        round = 0;
        currentStepCreature = 0;
        creatures = new ArrayList<>();
        map = new ArrayList<>();
        height = lines.size();
        if(height > 0){
            width = lines.get(0).length();
        }

        for(int j = 0; j < lines.size(); j++){
            String line = lines.get(j);
            List<MapElement> row = new ArrayList<>();
            for(int i = 0; i < line.length(); i++){
                if(MapElementFactory.isMapElement(line.charAt(i))) {
                    row.add(MapElementFactory.produce(line.charAt(i), new Point(i, j)));
                } else {
                    row.add(MapElementFactory.produce('.', new Point(i, j)));
                    creatures.add(MapElementFactory.produceCreature(line.charAt(i), new Point(i, j)));
                }
            }
            map.add(row);
        }

        highlights = new ArrayList<>();
    }

    public boolean isFree(Point position){
        if(getMapElementAt(position) instanceof Wall){
            return false;
        }
        for(Creature creature : creatures){
            if (creature.getPosition().isTheSame(position)){
                return false;
            }
        }
        return true;
    }

    public Map<Point, Integer> getAllReachableWithDistance(Point source){
        int distance = 0;
        Map<Point, Integer> reachable = new HashMap<>();
        List<Point> adjacentReachable = source
                                    .allAdjacent()
                                    .stream()
                                    .filter(this::isFree)
                                    .collect(Collectors.toList());

        while(!adjacentReachable.isEmpty()){
            for(Point adjacent : adjacentReachable){
                reachable.put(adjacent, distance);
            }
            adjacentReachable = adjacentReachable
                                    .stream()
                                    .flatMap(p-> p.allAdjacent().stream().filter(this::isFree))
                                    .distinct()
                                    .collect(Collectors.toList());
            adjacentReachable.removeAll(reachable.keySet());
            distance++;
        }

        return reachable;
    }

    public MapElement getMapElementAt(Point position){
        return map.get(position.getY()).get(position.getX());
    }

    public void makeSmallStep() {
        if(endGame)
            return;
        highlights.clear();
        if(currentStepCreature == 0){
            sortCreatures();
        }
        Creature currentCreature = creatures.get(currentStepCreature);
        currentCreature.makeMove(this);
        creatures.removeIf(c->c.getHp() <= 0);
        int nextIndex = creatures.indexOf(currentCreature) + 1;
        currentStepCreature = nextIndex%creatures.size();
        if(allCreaturesSameType()){
            int hpSum = creatures.stream().mapToInt(Creature::getHp).sum();
            System.out.println(hpSum*round);
            System.out.println(round);
            System.out.println(hpSum);
            System.out.println(creatures.stream().map(Creature::getHp).collect(Collectors.toList()));
            endGame =true;
        }
        if(currentStepCreature == 0){
            round++;
        }
    }

    private boolean allCreaturesSameType(){
        return creatures.stream().map(Creature::getType).distinct().collect(Collectors.toList()).size()==1;
    }

    private void sortCreatures(){
        creatures.sort((c1, c2)-> new PointsComparator().compare(c1.getPosition(), c2.getPosition()));
    }

    public void addHighlight(Highlight mapElement){
        highlights.add(mapElement);
    }
}
