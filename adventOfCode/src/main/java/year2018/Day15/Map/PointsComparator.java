package year2018.Day15.Map;

import year2018.Day15.Point;

import java.util.Comparator;

public class PointsComparator implements Comparator<Point> {
    @Override
    public int compare(Point o1, Point o2) {
        if(o1.getY() != o2.getY())
            return Integer.compare(o1.getY(), o2.getY());
        return Integer.compare(o1.getX(), o2.getX());
    }
}
