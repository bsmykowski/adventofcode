package year2018.Day15.Map;

import year2018.Day15.Map.Elements.*;
import year2018.Day15.Map.Elements.Creatures.Creature;
import year2018.Day15.Map.Elements.Creatures.Elf;
import year2018.Day15.Map.Elements.Creatures.Goblin;
import year2018.Day15.Point;

public class MapElementFactory {

    public static MapElement produce(Character character, Point position){

        switch(character){
            case '.':
                return new Floor(position);
            case '#':
                return new Wall(position);
            default:
                return new Floor(position);
        }

    }

    public static Creature produceCreature(Character character, Point position){

        switch(character){
            case 'G':
                return new Goblin(position);
            case 'E':
                return new Elf(position);
            default:
                return new Goblin(position);
        }

    }

    public static boolean isMapElement(Character character){
        return character.equals('#') || character.equals('.');
    }

}
