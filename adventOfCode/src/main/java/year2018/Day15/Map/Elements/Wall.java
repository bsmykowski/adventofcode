package year2018.Day15.Map.Elements;

import javafx.scene.paint.Color;
import lombok.Data;
import year2018.Day15.Point;

@Data
public class Wall extends MapElement {

    public Wall(Point position){
        this.position = position;
        this.color = Color.BLACK;
    }

}
