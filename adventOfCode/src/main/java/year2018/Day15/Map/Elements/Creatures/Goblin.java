package year2018.Day15.Map.Elements.Creatures;

import javafx.scene.paint.Color;
import year2018.Day15.Point;

public class Goblin extends Creature {

    public Goblin(Point position){
        super();
        this.position = position;
        this.color = Color.color(hp/200.0,hp/200.0,hp/200.0);
        this.type = CreatureType.GOBLIN;
    }

    @Override
    protected CreatureType targetCreatureType() {
        return CreatureType.ELF;
    }

    @Override
    protected void updateColor() {
        color = Color.color(hp/200.0,hp/200.0,hp/200.0);
    }
}
