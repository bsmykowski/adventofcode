package year2018.Day15.Map.Elements;

import javafx.scene.paint.Color;
import year2018.Day15.Point;

public class Highlight extends MapElement {
    public Highlight(Point position, Color color){
        this.position = position;
        this.color = color;
    }
}
