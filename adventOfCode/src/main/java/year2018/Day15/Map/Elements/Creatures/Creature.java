package year2018.Day15.Map.Elements.Creatures;

import javafx.scene.paint.Color;
import lombok.Data;
import lombok.EqualsAndHashCode;
import year2018.Day15.Direction;
import year2018.Day15.Map.Elements.Highlight;
import year2018.Day15.Map.Elements.MapElement;
import year2018.Day15.Map.GameMap;
import year2018.Day15.Map.PointsComparator;
import year2018.Day15.Point;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class Creature extends MapElement {

    protected CreatureType type;
    protected int hp;
    protected int attack;

    public Creature(){
        hp = 200;
        attack = 3;
    }

    public void makeMove(GameMap map){
        List<Creature> adjacentTargets = getAdjacentTargets(map);
        if(!adjacentTargets.isEmpty()){
            attack(map, adjacentTargets);
        } else {
            move(map);
            List<Creature> adjacentTargetsAfterMove = getAdjacentTargets(map);
            if(!adjacentTargetsAfterMove.isEmpty()) {
                attack(map, adjacentTargetsAfterMove);
            }
        }
    }

    private void move(GameMap map) {
        List<Point> freeAdjacentPoints = getPointsAdjacentToTargets(map);

        Map<Point, Integer> reachableWithDistance = map.getAllReachableWithDistance(position);

        List<Point> adjacentReachable = getAdjacentReachable(freeAdjacentPoints, reachableWithDistance);

        List<Point> closest = getClosestTargetLocations(reachableWithDistance, adjacentReachable);

        if(!closest.isEmpty()) {
            Point target = closest.get(0);
            List<Point> closestFirstMoves = getClosestFirstMoves(map, target);
            if(!closestFirstMoves.isEmpty()) {
                closestFirstMoves.sort(new PointsComparator());
                position = closestFirstMoves.get(0);
            }
        }
        map.addHighlight(new Highlight(this.position, Color.YELLOW));
    }

    private List<Point> getClosestFirstMoves(GameMap map, Point target) {
        Map<Point, Integer> distancesToTarget = map.getAllReachableWithDistance(target);
        List<Point> firstMoves = getPosition()
                                    .allAdjacent()
                                    .stream()
                                    .filter(map::isFree)
                                    .collect(Collectors.toList());
        int smallestDistance = Integer.MAX_VALUE;
        List<Point> closestFirstMoves = new ArrayList<>();
        for(Point firstMove : firstMoves){
            if(firstMove.isTheSame(target)){
                closestFirstMoves.clear();
                closestFirstMoves.add(firstMove);
                break;
            }
            if(!distancesToTarget.containsKey(firstMove))
                continue;
            Integer distance = distancesToTarget.get(firstMove);
            if(distance < smallestDistance){
                closestFirstMoves.clear();
                smallestDistance = distance;
                closestFirstMoves.add(firstMove);
            }
            else if(distance == smallestDistance){
                closestFirstMoves.add(firstMove);
            }
        }
        return closestFirstMoves;
    }

    private void attack(GameMap map, List<Creature> adjacentCreatures) {
        map.addHighlight(new Highlight(this.position, Color.YELLOW));
        int smallestHp = Integer.MAX_VALUE;
        List<Creature> weakestEnemies = new ArrayList<>();
        for(Creature creature:adjacentCreatures){
            if (creature.hp < smallestHp){
                weakestEnemies.clear();
                weakestEnemies.add(creature);
                smallestHp = creature.hp;
            } else if(creature.hp == smallestHp){
                weakestEnemies.add(creature);
            }
        }
        weakestEnemies.sort((c1, c2)->new PointsComparator().compare(c1.getPosition(), c2.getPosition()));
        if(!weakestEnemies.isEmpty()){
            weakestEnemies.get(0).decreaseHp(attack);
            map.addHighlight(new Highlight(weakestEnemies.get(0).getPosition(), Color.RED));
        }
    }

    private List<Point> getClosestTargetLocations(Map<Point, Integer> reachableWithDistance, List<Point> adjacentReachable) {
        List<Point> closest = new ArrayList<>();
        int closestDist = Integer.MAX_VALUE;
        for(Point point: adjacentReachable){
            if(reachableWithDistance.get(point) < closestDist){
                closest.clear();
                closest.add(point);
                closestDist = reachableWithDistance.get(point);
            } else if(reachableWithDistance.get(point) == closestDist){
                closest.add(point);
            }
        }

        closest.sort(new PointsComparator());
        return closest;
    }

    private List<Point> getAdjacentReachable(List<Point> freeAdjacentPoints, Map<Point, Integer> reachableWithDistance) {
        return reachableWithDistance
                    .keySet()
                    .stream()
                    .filter(freeAdjacentPoints::contains)
                    .collect(Collectors.toList());
    }

    private List<Point> getPointsAdjacentToTargets(GameMap map) {
        Stream<Creature> targets = map
                .getCreatures()
                .stream()
                .filter(c->c.type.equals(targetCreatureType()));

        Stream<Point> adjacentToTargets = targets
                .flatMap(c-> {
                            List<Point> adjacent = new ArrayList<>();
                            Point targetPosition = c.getPosition();
                            adjacent.add(targetPosition.getNextTo(Direction.LEFT));
                            adjacent.add(targetPosition.getNextTo(Direction.RIGHT));
                            adjacent.add(targetPosition.getNextTo(Direction.UP));
                            adjacent.add(targetPosition.getNextTo(Direction.DOWN));
                            return adjacent.stream();
                        });

        return adjacentToTargets
                .filter(map::isFree)
                .collect(Collectors.toList());
    }

    private List<Creature> getAdjacentTargets(GameMap map) {
        return map
                    .getCreatures()
                    .stream()
                    .filter(c->c.getPosition().isAdjacent(position)
                                && c.type.equals(targetCreatureType()))
                    .collect(Collectors.toList());
    }

    protected abstract CreatureType targetCreatureType();

    protected void decreaseHp(int amount){
        hp -= amount;
        if(hp > 0) {
            updateColor();
        }
    }

    protected abstract void updateColor();

}
