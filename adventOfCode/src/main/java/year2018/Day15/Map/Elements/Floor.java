package year2018.Day15.Map.Elements;

import javafx.scene.paint.Color;
import year2018.Day15.Point;

public class Floor extends MapElement {
    public Floor(Point position){
        this.position = position;
        this.color = Color.BROWN;
    }
}
