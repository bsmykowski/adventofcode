package year2018.Day15.Map.Elements;

import javafx.scene.paint.Color;
import lombok.Data;
import year2018.Day15.Point;

@Data
public abstract class MapElement {
    protected Point position;
    protected Color color;

    public int getX(){
        return position.getX();
    }

    public int getY(){
        return position.getY();
    }
}
