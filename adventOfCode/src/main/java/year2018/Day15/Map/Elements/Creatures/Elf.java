package year2018.Day15.Map.Elements.Creatures;

import javafx.scene.paint.Color;
import year2018.Day15.Point;

public class Elf extends Creature {

    public Elf(Point position){
        super();
        this.position = position;
        this.color = Color.color(0,hp/200.0,0);
        this.type = CreatureType.ELF;
        this.attack = 19;
    }

    @Override
    protected CreatureType targetCreatureType() {
        return CreatureType.GOBLIN;
    }

    @Override
    protected void updateColor() {
        color = Color.color(0,hp/200.0,0);
    }
}
