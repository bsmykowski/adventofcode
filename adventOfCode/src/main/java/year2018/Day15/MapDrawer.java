package year2018.Day15;

import javafx.scene.canvas.GraphicsContext;
import year2018.Day15.Map.Elements.Creatures.Creature;
import year2018.Day15.Map.Elements.Highlight;
import year2018.Day15.Map.GameMap;
import year2018.Day15.Map.Elements.MapElement;

public class MapDrawer {

    public void draw(GameMap map, double scale, GraphicsContext graphicsContext2D){

        for(int j = 0; j < map.getHeight(); j++){
            for(int i = 0; i < map.getWidth(); i++){
                MapElement mapElement = map.getMapElementAt(new Point(i,j));
                drawFilledRect(scale, graphicsContext2D, mapElement);
            }
        }
        for (Creature creature:map.getCreatures()){
            drawFilledRect(scale, graphicsContext2D, creature);
        }

        for(Highlight mapElement : map.getHighlights()){
            drawRect(scale, graphicsContext2D, mapElement);
        }

    }

    private void drawFilledRect(double scale, GraphicsContext graphicsContext2D, MapElement mapElement) {
        graphicsContext2D.setFill(mapElement.getColor());
        graphicsContext2D.fillRect(mapElement.getX() * scale, mapElement.getY() * scale, scale, scale);
    }

    private void drawRect(double scale, GraphicsContext graphicsContext2D, MapElement mapElement) {
        graphicsContext2D.setLineWidth(3.0);
        graphicsContext2D.setStroke(mapElement.getColor());
        graphicsContext2D.strokeRect(mapElement.getX() * scale, mapElement.getY() * scale, scale, scale);
    }

}
