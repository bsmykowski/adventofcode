package year2018.Day15;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Point {
    private int x;
    private int y;

    public int manhattanDistance(Point point){
        return Math.abs(point.x-x) + Math.abs(point.y - y);
    }

    public boolean isTheSame(Point point){
        return point.x==x && point.y ==y;
    }

    public Point getNextTo(Direction direction){
        switch(direction){
            case LEFT:
                return new Point(x-1, y);
            case RIGHT:
                return new Point(x+1, y);
            case UP:
                return new Point(x, y-1);
            case DOWN:
                return new Point(x, y+1);
            default:
                return new Point(x, y);
        }
    }

    public List<Point> allAdjacent(){
        List<Point> adjacent = new ArrayList<>();
        adjacent.add(getNextTo(Direction.UP));
        adjacent.add(getNextTo(Direction.DOWN));
        adjacent.add(getNextTo(Direction.LEFT));
        adjacent.add(getNextTo(Direction.RIGHT));
        return adjacent;
    }

    public boolean isAdjacent(Point point){
        return point.x == x && point.y == y-1
                || point.x == x && point.y == y+1
                || point.x == x-1 && point.y == y
                || point.x == x+1 && point.y == y;
    }
}
