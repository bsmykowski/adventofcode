package year2018.Day15;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import util.FilesReader;
import util.GlobalConstatns;
import year2018.Day15.Map.GameMap;

import java.util.List;

public class Day15 extends Application {
    private static final double SCALE = 20;
    private static final double WIDTH = 40;
    private static final double HEIGHT = 40;

    @Override
    public void start(Stage primaryStage) throws Exception {

        String path = GlobalConstatns.getPath(2018, 15);
        List<String> lines = FilesReader.getLines(path);

        GameMap map = new GameMap(lines);

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(WIDTH * SCALE, HEIGHT * SCALE);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root, WIDTH * SCALE, HEIGHT * SCALE));
        primaryStage.show();

        AnimationTimer timer = new AnimationTimer() {

            private long lastUpdate;

            @Override
            public void start() {
                lastUpdate = System.nanoTime();
                super.start();
            }

            @Override
            public void handle(long now) {
                double seconds = .01;
                if(now - lastUpdate > 1000000000*seconds) {
                    map.makeSmallStep();
                    lastUpdate = now;
                    drawCanvas(map, canvas);
                }
            }
        };
        drawCanvas(map, canvas);
        timer.start();
    }


    private void drawCanvas(GameMap map, Canvas canvas) {
        MapDrawer mapDrawer = new MapDrawer();
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        graphicsContext2D.setFill(Color.WHITE);
        graphicsContext2D.fillRect(0, 0, WIDTH*SCALE, HEIGHT*SCALE);
        mapDrawer.draw(map, SCALE, graphicsContext2D);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
