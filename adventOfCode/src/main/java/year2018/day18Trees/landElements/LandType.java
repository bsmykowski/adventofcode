package year2018.day18Trees.landElements;

public enum LandType {
    TREE, OPEN, LUMBERYARD
}
