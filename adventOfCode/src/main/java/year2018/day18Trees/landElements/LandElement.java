package year2018.day18Trees.landElements;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import year2018.day18Trees.Point;

@Data
@AllArgsConstructor
public abstract class LandElement {
    private Color color;
    private Point position;
    private LandType type;

    public int getX(){
        return position.getX();
    }

    public int getY(){
        return position.getY();
    }
}
