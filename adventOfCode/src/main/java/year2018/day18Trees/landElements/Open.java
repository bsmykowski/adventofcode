package year2018.day18Trees.landElements;

import javafx.scene.paint.Color;
import year2018.day18Trees.Point;

public class Open extends LandElement {
    public Open(Point position) {
        super(Color.SANDYBROWN, position, LandType.OPEN);
    }
}
