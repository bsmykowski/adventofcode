package year2018.day18Trees.landElements;

import javafx.scene.paint.Color;
import year2018.day18Trees.Point;

public class Tree extends LandElement {
    public Tree(Point position) {
        super(Color.DARKGREEN, position, LandType.TREE);
    }
}
