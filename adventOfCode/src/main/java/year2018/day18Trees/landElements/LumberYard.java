package year2018.day18Trees.landElements;

import javafx.scene.paint.Color;
import year2018.day18Trees.Point;

public class LumberYard extends LandElement {
    public LumberYard(Point position) {
        super(Color.BROWN, position, LandType.LUMBERYARD);
    }
}
