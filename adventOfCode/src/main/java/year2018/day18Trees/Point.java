package year2018.day18Trees;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class Point {
    private int x;
    private int y;

    public Point getNextTo(Direction direction){
        switch(direction){
            case LEFT:
                return new Point(x-1, y);
            case RIGHT:
                return new Point(x+1, y);
            case UP:
                return new Point(x, y-1);
            case DOWN:
                return new Point(x, y+1);
            case LEFT_UP:
                return new Point(x-1, y-1);
            case LEFT_DOWN:
                return new Point(x-1, y+1);
            case RIGHT_UP:
                return new Point(x+1, y-1);
            case RIGHT_DOWN:
                return new Point(x+1, y+1);
            default:
                return new Point(x, y);
        }
    }

    public List<Point> allAdjacent(){
        List<Point> adjacent = new ArrayList<>();
        adjacent.add(getNextTo(Direction.UP));
        adjacent.add(getNextTo(Direction.DOWN));
        adjacent.add(getNextTo(Direction.LEFT));
        adjacent.add(getNextTo(Direction.RIGHT));
        adjacent.add(getNextTo(Direction.RIGHT_DOWN));
        adjacent.add(getNextTo(Direction.RIGHT_UP));
        adjacent.add(getNextTo(Direction.LEFT_DOWN));
        adjacent.add(getNextTo(Direction.LEFT_UP));
        return adjacent;
    }
}
