package year2018.day18Trees;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import util.FilesReader;
import util.GlobalConstatns;

import java.util.List;

public class Day18 extends Application {
    private static final double SCALE = 10;
    private static final double SCENE_HEIGHT = 1000;
    private static final double SCENE_WIDTH = 1000;

    @Override
    public void start(Stage primaryStage) throws Exception {

        String path = GlobalConstatns.getPath(2018, "day18Trees");
        List<String> lines = FilesReader.getLines(path);

        System.out.println(countScore(1000000000, lines));

        Land land = new Land(lines);

        int width = land.getWidth();
        int height = land.getHeight();

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(width * SCALE, height * SCALE);
        ScrollPane scrollPane = new ScrollPane(root);
        scrollPane.setHvalue(0);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(scrollPane, SCENE_WIDTH, SCENE_HEIGHT));
        primaryStage.show();

        AnimationTimer timer = new AnimationTimer() {

            private long lastUpdate;

            @Override
            public void start() {
                lastUpdate = System.nanoTime();
                super.start();
            }

            @Override
            public void handle(long now) {
                double seconds = 0.1;
                if (now - lastUpdate > 1000000000 * seconds) {
                    for(int i = 0; i < 1; i++) {
                        land.doStep();
                    }
                    lastUpdate = now;
                    drawCanvas(land, canvas);
                }
            }
        };
        drawCanvas(land, canvas);
        timer.start();
    }

    public int countScore(int steps, List<String> lines){
        int t1 = 1000;
        Land newLand = new Land(lines);
        Land newLand2 = new Land(lines);
        for(int i = 0; i < t1; i++){
            newLand.doStep();
            newLand2.doStep();
        }

        newLand2.doStep();
        int deltaT = 1;
        while(!newLand.equals(newLand2)){
            deltaT++;
            newLand2.doStep();
        }

        int n = (steps - t1)/deltaT;
        int x = steps - deltaT * n - t1;

        for(int i = 0; i < x; i++){
            newLand.doStep();
        }

        return newLand.countScore();

    }


    private void drawCanvas(Land land, Canvas canvas) {
        LandDrawer landDrawer = new LandDrawer(land.getHeight(), land.getWidth(), SCALE);
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        landDrawer.draw(land, SCALE, graphicsContext2D);
    }

    public static void main(String[] args) {
        launch(args);
    }
}