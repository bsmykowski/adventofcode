package year2018.day18Trees;

import lombok.Data;
import year2018.day18Trees.landElements.LandElement;
import year2018.day18Trees.landElements.LumberYard;
import year2018.day18Trees.landElements.Open;
import year2018.day18Trees.landElements.Tree;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class Land {
    private int step;
    private int width;
    private int height;
    private List<List<LandElement>> land = new ArrayList<>();

    public Land(List<String> lines) {
        step = 0;
        height = lines.size();
        for(int j = 0; j < lines.size(); j++){
            String line = lines.get(j);
            List<LandElement> row = new ArrayList<>();
            for(int i = 0; i < line.length(); i++){
                switch(line.charAt(i)){
                    case '.':
                        row.add(new Open(new Point(i, j)));
                        break;
                    case '|':
                        row.add(new Tree(new Point(i, j)));
                        break;
                    case '#':
                        row.add(new LumberYard(new Point(i, j)));
                        break;
                }
            }
            land.add(row);
        }
        if(height > 0){
            width = lines.get(0).length();
        }
    }

    public void doStep(){
        List<List<LandElement>> newLand = new ArrayList<>();

        for(List<LandElement> row :land){
            List<LandElement> newRow = new ArrayList<>();
            for(LandElement landElement: row){
                List<Point> adjacent = landElement
                        .getPosition()
                        .allAdjacent()
                        .stream()
                        .filter(this::isInBound)
                        .collect(Collectors.toList());
                if(landElement instanceof Open) {
                    int treeCount = 0;
                    for (Point point : adjacent) {
                        if(getLandElement(point) instanceof Tree){
                            treeCount++;
                        }
                    }
                    if(treeCount >= 3){
                        newRow.add(new Tree(landElement.getPosition()));
                    } else {
                        newRow.add(landElement);
                    }
                } else if(landElement instanceof Tree) {
                    int lumberyardCount = 0;
                    for (Point point : adjacent) {
                        if(getLandElement(point) instanceof LumberYard){
                            lumberyardCount++;
                        }
                    }
                    if(lumberyardCount >= 3){
                        newRow.add(new LumberYard(landElement.getPosition()));
                    } else {
                        newRow.add(landElement);
                    }
                } else if(landElement instanceof LumberYard) {
                    int lumberyardCount = 0;
                    int treeCount = 0;
                    for (Point point : adjacent) {
                        if(getLandElement(point) instanceof LumberYard){
                            lumberyardCount++;
                        }
                        else if(getLandElement(point) instanceof Tree){
                            treeCount++;
                        }
                    }
                    if(lumberyardCount >= 1 && treeCount >= 1){
                        newRow.add(landElement);
                    } else {
                        newRow.add(new Open(landElement.getPosition()));
                    }
                }
            }
            newLand.add(newRow);
        }
        land = newLand;
        step++;
    }

    public int countScore(){
        int treeCount = 0;
        int lumberyardCount = 0;
        for(List<LandElement> row :land) {
            for (LandElement landElement : row) {
                if(landElement instanceof LumberYard){
                    lumberyardCount++;
                }
                else if(landElement instanceof Tree){
                    treeCount++;
                }
            }
        }
        return treeCount * lumberyardCount;
    }

    private boolean isInBound(Point point) {
        return point.getX() >= 0 && point.getX() < width && point.getY() >= 0 && point.getY() < height;
    }

    public LandElement getLandElement(Point position){
        return land.get(position.getY()).get(position.getX());
    }

    public void setLandElement(LandElement landElement){
        land.get(landElement.getPosition().getY()).set(landElement.getPosition().getX(), landElement);
    }

    public boolean equals(Land land){
        if(land.height != this.height || land.width != this.width){
            return false;
        }
        for(int j = 0; j < height; j++) {
            for (int i = 0; i < width; i++) {
                if(!getLandElement(new Point(i, j)).getType().equals(land.getLandElement(new Point(i,j)).getType())) {
                    return false;
                }
            }
        }
        return true;
    }

}
