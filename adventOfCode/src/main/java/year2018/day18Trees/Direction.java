package year2018.day18Trees;

public enum Direction {
    LEFT, RIGHT, UP, DOWN, NONE, LEFT_UP, LEFT_DOWN, RIGHT_UP, RIGHT_DOWN
}
