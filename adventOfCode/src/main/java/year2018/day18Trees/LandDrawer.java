package year2018.day18Trees;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import year2018.day18Trees.landElements.LandElement;

import java.util.List;

@Data
@AllArgsConstructor
public class LandDrawer {

    private double height;
    private double width;
    private double scale;

    public void draw(Land land, double scale, GraphicsContext graphicsContext2D){

        graphicsContext2D.setFill(Color.WHITE);
        graphicsContext2D.fillRect(0, 0, width*scale, height*scale);

        for(List<LandElement> row :land.getLand()){
            for(LandElement landElement: row){
                drawFilledRect(scale, graphicsContext2D, landElement);
            }
        }

    }

    private void drawFilledRect(double scale, GraphicsContext graphicsContext2D, LandElement mapElement) {
        graphicsContext2D.setFill(mapElement.getColor());
        graphicsContext2D.fillRect(mapElement.getX() * scale, mapElement.getY() * scale, scale, scale);
    }

    private void drawRect(double scale, GraphicsContext graphicsContext2D, LandElement mapElement) {
        graphicsContext2D.setLineWidth(3.0);
        graphicsContext2D.setStroke(mapElement.getColor());
        graphicsContext2D.strokeRect(mapElement.getX() * scale, mapElement.getY() * scale, scale, scale);
    }

}
