package year2018.Day16;


import lombok.Getter;
import year2018.Day16.operations.Operation;

import java.util.ArrayList;
import java.util.List;

public class Registers {
    @Getter
    private List<Long> registers = new ArrayList<>();

    public Registers(String state){
        String[] tokens = state.split(",");
        for (String token : tokens) {
            registers.add(Long.parseLong(token.trim()));
        }
    }

    public Registers(int size){
        registers = new ArrayList<>();
        for(int i = 0; i < size; i++){
            registers.add(0L);
        }
    }

    public void performOperation(Operation operation){
        operation.compute(this);
    }

    public long get(int index){
        return registers.get(index);
    }

    public void set(int index, long value){
        registers.set(index, value);
    }

    public boolean equals(String state){
        String[] tokens = state.split(",");
        if(tokens.length != registers.size()){
            return false;
        }
        for(int i = 0; i < tokens.length; i++){
            if(registers.get(i) != Integer.parseInt(tokens[i].trim())){
                return false;
            }
        }
        return true;
    }

}
