package year2018.Day16.operations;

import year2018.Day16.Registers;

public interface Operation {
    void compute(Registers registers);
}
