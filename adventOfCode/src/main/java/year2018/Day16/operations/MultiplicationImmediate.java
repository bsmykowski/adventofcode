package year2018.Day16.operations;

import year2018.Day16.Registers;

public class MultiplicationImmediate extends OperationBase {
    public MultiplicationImmediate(OperationArguments arguments) {
        super(arguments);
    }

    @Override
    public long getResult(Registers registers) {
        return registers.get(firstArg) * secondArg;
    }
}
