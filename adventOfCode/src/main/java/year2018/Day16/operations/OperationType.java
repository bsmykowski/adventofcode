package year2018.Day16.operations;

public enum OperationType {
    ADDITION_IMMEDIATE,
    ADDITION_REGISTER,
    ASSIGNMENT_IMMEDIATE,
    ASSIGNMENT_REGISTER,
    BITWISE_AND_IMMEDIATE,
    BITWISE_AND_REGISTER,
    BITWISE_OR_IMMEDIATE,
    BITWISE_OR_REGISTER,
    EQUALITY_TESTING_IMMEDIATE_REGISTER,
    EQUALITY_TESTING_REGISTER_IMMEDIATE,
    EQUALITY_TESTING_REGISTER_REGISTER,
    GREATER_THAN_TESTING_IMMEDIATE_REGISTER,
    GREATER_THAN_TESTING_REGISTER_IMMEDIATE,
    GREATER_THAN_TESTING_REGISTER_REGISTER,
    MULTIPLICATION_IMMEDIATE,
    MULTIPLICATION_REGISTER;

    public static Operation getOperation(OperationType operationType, OperationArguments arguments){
        switch(operationType){
            case ADDITION_IMMEDIATE:
                return new AdditionImmediate(arguments);
            case ADDITION_REGISTER:
                return new AdditionRegister(arguments);
            case ASSIGNMENT_IMMEDIATE:
                return new AssignmentImmediate(arguments);
            case ASSIGNMENT_REGISTER:
                return new AssignmentRegister(arguments);
            case BITWISE_AND_IMMEDIATE:
                return new BitwiseAndImmediate(arguments);
            case BITWISE_AND_REGISTER:
                return new BitwiseAndRegister(arguments);
            case BITWISE_OR_IMMEDIATE:
                return new BitwiseOrImmediate(arguments);
            case BITWISE_OR_REGISTER:
                return new BitwiseOrRegister(arguments);
            case EQUALITY_TESTING_IMMEDIATE_REGISTER:
                return new EqualityTestingImmediateRegister(arguments);
            case EQUALITY_TESTING_REGISTER_IMMEDIATE:
                return new EqualityTestingRegisterImmediate(arguments);
            case EQUALITY_TESTING_REGISTER_REGISTER:
                return new EqualityTestingRegisterRegister(arguments);
            case GREATER_THAN_TESTING_IMMEDIATE_REGISTER:
                return new GreaterThanTestingImmediateRegister(arguments);
            case GREATER_THAN_TESTING_REGISTER_IMMEDIATE:
                return new GreaterThanTestingRegisterImmediate(arguments);
            case GREATER_THAN_TESTING_REGISTER_REGISTER:
                return new GreaterThanTestingRegisterRegister(arguments);
            case MULTIPLICATION_IMMEDIATE:
                return new MultiplicationImmediate(arguments);
            case MULTIPLICATION_REGISTER:
                return new MultiplicationRegister(arguments);
        }
        throw new RuntimeException();
    }

}
