package year2018.Day16.operations;

import year2018.Day16.Registers;

public class AssignmentImmediate extends OperationBase {
    public AssignmentImmediate(OperationArguments arguments) {
        super(arguments);
    }

    @Override
    public long getResult(Registers registers) {
        return firstArg;
    }
}
