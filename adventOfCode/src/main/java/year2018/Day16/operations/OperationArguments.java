package year2018.Day16.operations;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OperationArguments {

    private int arg1;
    private int arg2;
    private int arg3;

}
