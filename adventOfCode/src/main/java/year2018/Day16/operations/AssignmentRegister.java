package year2018.Day16.operations;

import year2018.Day16.Registers;

public class AssignmentRegister extends OperationBase {
    public AssignmentRegister(OperationArguments arguments) {
        super(arguments);
    }

    @Override
    public long getResult(Registers registers) {
        return registers.get(firstArg);
    }
}
