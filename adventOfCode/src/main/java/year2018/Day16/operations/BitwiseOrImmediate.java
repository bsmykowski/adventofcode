package year2018.Day16.operations;

import year2018.Day16.Registers;

public class BitwiseOrImmediate extends OperationBase {
    public BitwiseOrImmediate(OperationArguments arguments) {
        super(arguments);
    }

    @Override
    public long getResult(Registers registers) {
        return registers.get(firstArg) | secondArg;
    }
}
