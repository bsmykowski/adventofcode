package year2018.Day16.operations;

import year2018.Day16.Registers;

public class EqualityTestingImmediateRegister extends OperationBase {
    public EqualityTestingImmediateRegister(OperationArguments arguments) {
        super(arguments);
    }

    @Override
    public long getResult(Registers registers) {
        return firstArg == registers.get(secondArg) ? 1 : 0;
    }
}
