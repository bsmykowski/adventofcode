package year2018.Day16.operations;

import year2018.Day16.Registers;

public class GreaterThanTestingImmediateRegister extends OperationBase {
    public GreaterThanTestingImmediateRegister(OperationArguments arguments) {
        super(arguments);
    }

    @Override
    public long getResult(Registers registers) {
        return firstArg > registers.get(secondArg) ? 1 : 0;
    }
}
