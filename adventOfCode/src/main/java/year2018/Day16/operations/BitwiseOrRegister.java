package year2018.Day16.operations;

import year2018.Day16.Registers;

public class BitwiseOrRegister extends OperationBase {
    public BitwiseOrRegister(OperationArguments arguments) {
        super(arguments);
    }

    @Override
    public long getResult(Registers registers) {
        return registers.get(firstArg) | registers.get(secondArg);
    }
}
