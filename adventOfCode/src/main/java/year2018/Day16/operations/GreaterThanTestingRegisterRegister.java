package year2018.Day16.operations;

import year2018.Day16.Registers;

public class GreaterThanTestingRegisterRegister extends OperationBase {
    public GreaterThanTestingRegisterRegister(OperationArguments arguments) {
        super(arguments);
    }

    @Override
    public long getResult(Registers registers) {
        return registers.get(firstArg) > registers.get(secondArg) ? 1 : 0;
    }
}
