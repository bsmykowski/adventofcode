package year2018.Day16.operations;

import lombok.Data;
import year2018.Day16.Registers;

@Data
public abstract class OperationBase implements Operation{
    protected int firstArg;
    protected int secondArg;
    protected int thirdArg;
    private int operationCode = -1;

    public OperationBase(OperationArguments arguments){
        firstArg = arguments.getArg1();
        secondArg = arguments.getArg2();
        thirdArg = arguments.getArg3();
    }

    @Override
    public void compute(Registers registers){
        registers.set(thirdArg, getResult(registers));
    }

    public abstract long getResult(Registers registers);
}
