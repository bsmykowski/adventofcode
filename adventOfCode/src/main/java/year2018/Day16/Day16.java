package year2018.Day16;

import javafx.util.Pair;
import util.FilesReader;
import util.GlobalConstatns;
import year2018.Day16.operations.Operation;
import year2018.Day16.operations.OperationArguments;
import year2018.Day16.operations.OperationType;

import java.util.*;

public class Day16 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2018, 16);
        List<String> lines = FilesReader.getLines(path);


        int indexOfLastAfterLine = 0;
        for(int i = 0; i < lines.size(); i++){
            String line = lines.get(i);
            if(line.startsWith("After")){
                indexOfLastAfterLine = i;
            }
        }
        int numberOfTests = (indexOfLastAfterLine+2)/4;


        Map<Integer, OperationType> operationsCodes = calculateOperationsCodes(lines, numberOfTests);


        int indexOfProgramFirstInstruction = indexOfLastAfterLine + 4;

        Registers registers = new Registers(4);
        for (int i = indexOfProgramFirstInstruction; i < lines.size(); i++){
            String[] operationDataTokens = lines.get(i).split(" ");

            int operationCode = Integer.parseInt(operationDataTokens[0]);
            int operationArg1 = Integer.parseInt(operationDataTokens[1]);
            int operationArg2 = Integer.parseInt(operationDataTokens[2]);
            int operationArg3 = Integer.parseInt(operationDataTokens[3]);
            OperationArguments arguments = new OperationArguments(operationArg1, operationArg2, operationArg3);

            Operation operation = OperationType.getOperation(operationsCodes.get(operationCode), arguments);
            operation.compute(registers);
        }


        for(Long register : registers.getRegisters()){
            System.out.println(register);
        }

    }

    private static Map<Integer, OperationType> calculateOperationsCodes(List<String> lines, int numberOfTests) {
        Map<Integer, OperationType> operationsCodes = new HashMap<>();

        Map<Integer, List<OperationType>> possibleOperations = new HashMap<>();
        for(int i  = 0; i < 16; i++){
            List<OperationType> operationTypes = new ArrayList<>(Arrays.asList(OperationType.values()));
            possibleOperations.put(i, operationTypes);
        }

        for(int i = 0; i < numberOfTests; i++){
            String setup = lines.get(i*4);
            String operation = lines.get(i*4+1);
            String result = lines.get(i*4+2);

            TestCase testCase = new TestCase(setup, result, operation);


            Pair<Integer, List<OperationType>> possibleOp = testCase.getPossibleOperations();

            for(int j = possibleOperations.get(testCase.getOperationCode()).size()-1; j >= 0; j--){
                if(!possibleOp.getValue().contains(possibleOperations.get(testCase.getOperationCode()).get(j))){
                    possibleOperations.get(testCase.getOperationCode()).remove(possibleOperations.get(testCase.getOperationCode()).get(j));
                }
            }

        }

        while(!empty(possibleOperations)){
            for(int i = 0; i < 16; i++) {
                if (possibleOperations.get(i).size() == 1) {
                    OperationType known = possibleOperations.get(i).get(0);
                    operationsCodes.put(i, known);
                    for (int j = 0; j < 16; j++) {
                        possibleOperations.get(j).remove(known);
                    }
                }
            }
        }
        return operationsCodes;
    }

    private static boolean empty(Map<Integer, List<OperationType>> possibleOperations){
        for(List<OperationType> operations : possibleOperations.values()){
            if(!operations.isEmpty()){
                return false;
            }
        }
        return true;
    }

}
