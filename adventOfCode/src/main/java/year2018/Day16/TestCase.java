package year2018.Day16;

import javafx.util.Pair;
import lombok.Data;
import year2018.Day16.operations.OperationArguments;
import year2018.Day16.operations.OperationType;

import java.util.ArrayList;
import java.util.List;

@Data
public class TestCase {
    private String before;
    private String after;
    private OperationArguments arguments;
    private int operationCode;

    public TestCase(String before, String after, String operation){
        this.before = before;
        this.after = after;
        String[] operationDataTokens = operation.split(" ");

        this.operationCode = Integer.parseInt(operationDataTokens[0]);
        int operationArg1 = Integer.parseInt(operationDataTokens[1]);
        int operationArg2 = Integer.parseInt(operationDataTokens[2]);
        int operationArg3 = Integer.parseInt(operationDataTokens[3]);
        this.arguments = new OperationArguments(operationArg1, operationArg2, operationArg3);
    }

    public int getNumberOfPossibleOperations(){
        int numberOfPositive = 0;

        for(OperationType operationType:OperationType.values()) {
            Registers registers = new Registers(stateString(before));
            registers.performOperation(OperationType.getOperation(operationType, arguments));
            if(registers.equals(stateString(after))){
                numberOfPositive++;
            }
        }

        return numberOfPositive;
    }

    public Pair<Integer, List<OperationType>> getPossibleOperations(){
        List<OperationType> possibleOperations = new ArrayList<>();
        Pair<Integer, List<OperationType>> result = new Pair<>(operationCode, possibleOperations);

        for(OperationType operationType:OperationType.values()) {
            Registers registers = new Registers(stateString(before));
            registers.performOperation(OperationType.getOperation(operationType, arguments));
            if(registers.equals(stateString(after))){
                possibleOperations.add(operationType);
            }
        }

        return result;
    }

    private static String stateString(String line){
        return line.substring(line.indexOf('[')+1, line.indexOf(']'));
    }
}
