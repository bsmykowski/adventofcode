package year2017.Day22Ant;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.List;

public class Day22 {

    public static void main(String[] args) {
        String filePath = GlobalConstatns.getPath(2017, "Day22Ant");
        List<String> lines = FilesReader.getLines(filePath);

        AntsMap map = new AntsMap(lines);
        for(int i =0; i <10000000; i++) {
            map.makeTurn();
        }
        //elements.print();
        System.out.println(map.howManyInfected());

    }

}
