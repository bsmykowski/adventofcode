package year2015.Day3;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.ArrayList;
import java.util.List;

public class Day3 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2015, "Day6");

        List<String> lines = FilesReader.getLines(path);

        List<Direction> directions = new ArrayList<>();
        for(String line : lines){
            for(Character character : line.toCharArray()) {
                directions.add(Direction.getDirection(character));
            }
        }

        Land map = new Land(10000);
        map.performActions(directions);
        System.out.println(map.getNumberOfVisited());
    }

}
