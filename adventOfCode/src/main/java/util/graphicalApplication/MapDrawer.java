package util.graphicalApplication;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class MapDrawer {


    public void draw(ObjectsMap objectsMap, double scale, GraphicsContext graphicsContext2D){

        graphicsContext2D.setFill(Color.WHITE);
        graphicsContext2D.fillRect(0, 0, objectsMap.getWidth()*scale, objectsMap.getHeight()*scale);

        for(GraphicalObject object:objectsMap.getObjectsContainer().getObjects().values()){
            switch(object.getType()){
                case FILLED_RECT:
                    drawFilledRect(scale, graphicsContext2D, object);
                    break;
                case RECT:
                    drawRect(scale, graphicsContext2D, object);
                    break;
            }
        }
    }

    private void drawFilledRect(double scale, GraphicsContext graphicsContext2D, GraphicalObject object) {
        Color color = object.getColor();
        graphicsContext2D.setFill(color);
        double x = object.getX() * scale;
        double y = object.getY() * scale;
        graphicsContext2D.fillRect(x, y, scale, scale);
    }

    private void drawRect(double scale, GraphicsContext graphicsContext2D, GraphicalObject object) {
        graphicsContext2D.setLineWidth(3.0);
        graphicsContext2D.setStroke(object.getColor());
        graphicsContext2D.strokeRect(object.getX() * scale, object.getY() * scale, scale, scale);
    }

}
