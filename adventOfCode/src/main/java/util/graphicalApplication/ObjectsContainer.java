package util.graphicalApplication;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ObjectsContainer {
    private Map<Point, GraphicalObject> objects = new HashMap<>();

    public void addObject(GraphicalObject object){
        objects.put(object.getPosition(), object);
    }

    public GraphicalObject getObject(Point point){
        return objects.get(point);
    }

    public Point getTopLeft(){
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        for(Point point:objects.keySet()){
            if(point.getX() < minX){
                minX = point.getX();
            }
            if (point.getY() < minY){
                minY = point.getY();
            }
        }
        return new Point(minX, minY);
    }

    public Point getDownRight(){
        int maxX = Integer.MIN_VALUE;
        int maxY = Integer.MIN_VALUE;
        for(Point point:objects.keySet()){
            if(point.getX() > maxX){
                maxX = point.getX();
            }
            if (point.getY() > maxY){
                maxY = point.getY();
            }
        }
        return new Point(maxX, maxY);
    }

}
