package util.graphicalApplication;

public abstract class AnimatedObjectsMap extends ObjectsMap {

    public abstract void tick();
}
