package util.graphicalApplication;

import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GraphicalObject {
    private Point position;
    private Color color;
    private GraphicalObjectTypes type;

    public int getX(){
        return position.getX();
    }

    public int getY(){
        return position.getY();
    }
}
