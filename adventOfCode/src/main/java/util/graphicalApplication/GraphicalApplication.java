package util.graphicalApplication;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public abstract class GraphicalApplication extends Application {
    private int scale;
    private int sceneWidth;
    private int sceneHeight;
    protected ObjectsMap objectsMap;
    protected Canvas canvas;

    public GraphicalApplication(int scale,
                                int sceneWidth,
                                int sceneHeight){
        this.scale = scale;
        this.sceneWidth = sceneWidth;
        this.sceneHeight = sceneHeight;
    }

    public abstract ObjectsMap initializeObjectsMap();

    public void afterSceneCreation(){}

    @Override
    public void start(Stage primaryStage) throws Exception {

        objectsMap = initializeObjectsMap();

        int canvasWidth = objectsMap.getWidth();
        int canvasHeight = objectsMap.getHeight();

        AnchorPane root = new AnchorPane();
        canvas = new Canvas(canvasWidth * scale, canvasHeight * scale);
        ScrollPane scrollPane = new ScrollPane(root);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(scrollPane, sceneWidth, sceneHeight));
        primaryStage.show();

        drawCanvas();
        afterSceneCreation();
    }

    protected void drawCanvas() {
        MapDrawer mapDrawer = new MapDrawer();
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        mapDrawer.draw(objectsMap, scale, graphicsContext2D);
    }

}
