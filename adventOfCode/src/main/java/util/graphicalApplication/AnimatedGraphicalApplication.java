package util.graphicalApplication;

import javafx.animation.AnimationTimer;

public abstract class AnimatedGraphicalApplication extends GraphicalApplication {

    public AnimatedGraphicalApplication(int scale, int sceneWidth, int sceneHeight) {
        super(scale, sceneWidth, sceneHeight);
    }

    @Override
    public ObjectsMap initializeObjectsMap(){
        return initializeAnimatedObjectsMap();
    }

    public abstract AnimatedObjectsMap initializeAnimatedObjectsMap();

    @Override
    public void afterSceneCreation(){
        AnimationTimer timer = new AnimationTimer() {

            private long lastUpdate;

            @Override
            public void start() {
                lastUpdate = System.nanoTime();
                super.start();
            }

            @Override
            public void handle(long now) {
                double seconds = 0.1;
                if (now - lastUpdate > 1000000000 * seconds) {
                    for(int i = 0; i < 1; i++) {
                        ((AnimatedObjectsMap)objectsMap).tick();
                    }
                    lastUpdate = now;
                    drawCanvas();
                }
            }
        };
        timer.start();
    }
}
