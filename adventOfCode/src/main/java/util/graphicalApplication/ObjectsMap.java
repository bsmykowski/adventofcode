package util.graphicalApplication;

import lombok.Data;

import java.util.List;

@Data
public abstract class ObjectsMap {
    protected ObjectsContainer objectsContainer;

    protected abstract ObjectsContainer generateContainer(List<String> lines);

    public Point getTopLeft(){
        return objectsContainer.getTopLeft();
    }

    public Point getDownRight(){
        return objectsContainer.getDownRight();
    }

    public int getWidth(){
        return getDownRight().getX() - getTopLeft().getX()+1;
    }

    public int getHeight(){
        return getDownRight().getY() - getTopLeft().getY()+1;
    }

}
