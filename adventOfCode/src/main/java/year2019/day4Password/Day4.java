package year2019.day4Password;

public class Day4 {

    public static void main(String[] args) {

        int start = 284639;
        int end = 748759;


        int counter = 0;
        for(int i = start; i <= end; i++){
            if(matchCriteria(i)){
                counter++;
            }
        }
        System.out.println(counter);

    }

    private static boolean matchCriteria(int password){
        String stringPassword = password + "";

        if(stringPassword.length() != 6){
            return false;
        }

        for(int i =0;i < 5; i++){

            char previous = stringPassword.charAt(i);
            char next = stringPassword.charAt(i+1);

            if(Integer.parseInt(previous + "") > Integer.parseInt(next+ "")){
                return false;
            }
        }

        return containsDouble(stringPassword);
    }

    private static boolean containsDouble(String password){

        for(int i =0;i < 5; i++) {

            char previous = password.charAt(i);
            char next = password.charAt(i + 1);
            if(i == 0){
                if(previous == next && next != password.charAt(i+2))
                    return true;
            } else if(i == 4){
                if(previous == next && previous != password.charAt(i-1))
                    return true;
            } else {
                if(previous == next && previous != password.charAt(i-1) && next != password.charAt(i+2))
                    return true;
            }
        }
        return false;
    }

}
