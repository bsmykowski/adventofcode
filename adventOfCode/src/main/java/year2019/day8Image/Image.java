package year2019.day8Image;

import javafx.scene.paint.Color;
import util.graphicalApplication.*;

import java.util.ArrayList;
import java.util.List;

public class Image extends ObjectsMap {

    private final int width;
    private final int height;
    private List<List<ImagePixel>> layers = new ArrayList<>();

    public Image(List<String> lines, int width, int height) {
        this.width = width;
        this.height = height;
        objectsContainer = generateContainer(lines);
    }

    @Override
    protected ObjectsContainer generateContainer(List<String> lines) {
        ObjectsContainer container = new ObjectsContainer();

        List<Integer> imageData = new ArrayList<>();

        for(String line: lines){
            for(Character character:line.toCharArray()){
                imageData.add(Integer.parseInt(character + ""));
            }
        }

        int layerSize = width*height;
        int numberOfLayers = imageData.size()/layerSize;

        for(int layerId = 0; layerId < numberOfLayers; layerId++) {
            List<ImagePixel> layer = new ArrayList<>();
            for (int j = 0; j < height; j++) {
                for (int i = 0; i < width; i++) {
                    int value = imageData.get(j * width + i + layerId*layerSize);
                    Color color = Color.color(value / 2.0, value / 2.0, value / 2.0);
                    ImagePixel imagePixel = new ImagePixel(new Point(i, j), color, value);
                    layer.add(imagePixel);
                }
            }
            layers.add(layer);
        }

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                for (List<ImagePixel> layer : layers) {
                    ImagePixel current = layer.get(j*width + i);
                    if(current.getValue() != 2){
                        Color color = Color.RED;
                        if(current.getValue() == 0){
                            color = Color.BLACK;
                        }
                        container.addObject(new ImagePixel(new Point(i, j), color, current.getValue()));
                        break;
                    }
                }
            }
        }
        return container;
    }

    public int getScoreOfBestLayer(){
        int minZeros = Integer.MAX_VALUE;
        int bestScore = 0;
        for(List<ImagePixel> layer : layers){
            int numberOfZeros = 0;
            int numberOfOnes = 0;
            int numberOfTwos = 0;
            for(ImagePixel pixel:layer){
                if(pixel.getValue() == 0){
                    numberOfZeros++;
                } else if(pixel.getValue() == 1){
                    numberOfOnes++;
                } else if(pixel.getValue() == 2){
                    numberOfTwos++;
                }
            }
            if(numberOfZeros < minZeros){
                minZeros = numberOfZeros;
                bestScore = numberOfOnes*numberOfTwos;
            }
        }
        return bestScore;
    }

}
