package year2019.day8Image;

import util.FilesReader;
import util.GlobalConstatns;
import util.graphicalApplication.GraphicalApplication;
import util.graphicalApplication.ObjectsMap;

import java.util.List;

public class Day8 extends GraphicalApplication {

    public Day8() {
        super(10, 400, 400);
    }

    @Override
    public ObjectsMap initializeObjectsMap() {
        String path = GlobalConstatns.getPath(2019, "day8Image");
        List<String> lines = FilesReader.getLines(path);
        return new Image(lines, 25,6);
    }

    @Override
    public void afterSceneCreation(){
        super.afterSceneCreation();
        System.out.println(((Image)objectsMap).getScoreOfBestLayer());
    }

    public static void main(String[] args) {
        launch(args);
    }

}
