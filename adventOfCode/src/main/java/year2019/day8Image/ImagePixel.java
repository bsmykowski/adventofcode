package year2019.day8Image;

import javafx.scene.paint.Color;
import lombok.Getter;
import util.graphicalApplication.GraphicalObject;
import util.graphicalApplication.GraphicalObjectTypes;
import util.graphicalApplication.Point;

public class ImagePixel extends GraphicalObject {
    @Getter
    private int value;

    public ImagePixel(Point position, Color color, int value) {
        super(position, color, GraphicalObjectTypes.FILLED_RECT);
        this.value = value;
    }

}
