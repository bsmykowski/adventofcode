package year2019.day7Processor3;

import util.FilesReader;
import util.GlobalConstatns;
import year2019.processor.Program;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day7 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2019, "day7Processor3");
        List<String> lines = FilesReader.getLines(path);

        List<Long> instructions = Arrays.stream(lines.get(0).split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());


        List<List<Integer>> allPhases = new ArrayList<>();
        List<Integer> beginning = new ArrayList<>();
        List<Integer> elements = new ArrayList<>();
        for(int i = 5; i < 10; i++){
            elements.add(i);
        }
        generatePermutations(beginning, elements, allPhases);

        long max = Long.MIN_VALUE;
        for(List<Integer> permutation: allPhases){

            List<Program> programs = new ArrayList<>();
            for(int i = 0; i < 1; i++) {
                for (Integer phaseSetting : permutation) {
                    Program program = new Program(new ArrayList<>(instructions));
                    program.addInput(phaseSetting);
                    programs.add(program);
                }
                long prevOutput = 0;
                while(!areStopped(programs)) {
                    for (Program program : programs) {
                        program.addInput(prevOutput);
                        program.run();
                        prevOutput = program.getLastOutput();
                    }
                }
            }

            long output = programs.get(programs.size()-1).getLastOutput();
            if(output > max){
                max = output;
            }

        }

        System.out.println(max);
        System.out.println("Should be 1714298");

    }

    public static boolean areStopped(List<Program> programs){
        for(Program program : programs){
            if(program.isRunning()){
                return false;
            }
        }
        return true;
    }

    private static void generatePermutations(List<Integer> beginning, List<Integer> left, List<List<Integer>> result){
        if(left.size() == 0){
            result.add(beginning);
            return;
        }
        for(int i = left.size()-1; i >= 0; i--){
            List<Integer> newLeft = new ArrayList<>(left);
            List<Integer> permutation = new ArrayList<>(beginning);
            newLeft.remove(left.get(i));
            permutation.add(left.get(i));
            generatePermutations(permutation, newLeft, result);
        }
    }


}
