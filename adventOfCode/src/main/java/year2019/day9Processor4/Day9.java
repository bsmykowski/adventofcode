package year2019.day9Processor4;

import util.FilesReader;
import util.GlobalConstatns;
import year2019.processor.Program;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day9 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2019, "day9Processor4");
        List<String> lines = FilesReader.getLines(path);

        List<Long> instructions = Arrays.stream(lines.get(0).split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());

        Program program = new Program(instructions, 2L);
        program.run();
        System.out.println(program.getOutputBuffer());

    }

}
