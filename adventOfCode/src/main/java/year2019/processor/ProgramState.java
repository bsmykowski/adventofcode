package year2019.processor;

public enum  ProgramState {
    BEFORE_START, RUNNING, STOP, END, WAIT_FOR_INPUT, JUMPING
}
