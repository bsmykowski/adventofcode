package year2019.processor.operations.oneParameter;

import year2019.processor.Program;
import year2019.processor.parameters.Parameter;

public class RelativeBaseOffset extends OneParameterOperation {
    public RelativeBaseOffset(Parameter parameter) {
        super(parameter);
    }

    @Override
    public void process(Program program) {
        program.setRelativeBase(program.getRelativeBase()+(int)parameter.getValue(program));
    }
}
