package year2019.processor.operations.oneParameter;

import year2019.processor.Program;
import year2019.processor.parameters.Parameter;

public class Output extends OneParameterOperation {

    public Output(Parameter parameter) {
        super(parameter);
    }

    @Override
    public void process(Program program) {
        program.getOutputBuffer().add(parameter.getValue(program));
    }

}
