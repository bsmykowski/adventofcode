package year2019.processor.operations.oneParameter;

import year2019.processor.operations.Operation;
import year2019.processor.parameters.Parameter;

public abstract class OneParameterOperation implements Operation {
    protected Parameter parameter;

    public OneParameterOperation(Parameter parameter){
        this.parameter = parameter;
    }
}
