package year2019.processor.operations.oneParameter;

import year2019.processor.Program;
import year2019.processor.ProgramState;
import year2019.processor.parameters.Parameter;

public class Input extends OneParameterOperation{
    public Input(Parameter parameter) {
        super(parameter);
    }

    @Override
    public void process(Program program) {
        if(!program.hasInput()){
            program.setState(ProgramState.WAIT_FOR_INPUT);
        } else {
            program.setAt(parameter.getIfDestination(program), program.consumeInput());
        }
    }
}
