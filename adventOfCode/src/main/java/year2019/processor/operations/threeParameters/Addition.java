package year2019.processor.operations.threeParameters;

import year2019.processor.Program;
import year2019.processor.parameters.Parameter;

public class Addition extends ThreeParametersOperation {

    public Addition(Parameter firstParam, Parameter secondParam, Parameter thirdParam) {
        super(firstParam, secondParam, thirdParam);
    }

    @Override
    public void process(Program program) {
        program.setAt(thirdParam.getIfDestination(program), firstParam.getValue(program) + secondParam.getValue(program));
    }
}
