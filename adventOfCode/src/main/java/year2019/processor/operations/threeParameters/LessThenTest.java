package year2019.processor.operations.threeParameters;

import year2019.processor.Program;
import year2019.processor.parameters.Parameter;

public class LessThenTest extends ThreeParametersOperation{
    public LessThenTest(Parameter firstParam, Parameter secondParam, Parameter thirdParam) {
        super(firstParam, secondParam, thirdParam);
    }

    @Override
    public void process(Program program) {
        if(firstParam.getValue(program) < secondParam.getValue(program)){
            program.setAt(thirdParam.getIfDestination(program), 1);
        } else {
            program.setAt(thirdParam.getIfDestination(program), 0);
        }
    }
}
