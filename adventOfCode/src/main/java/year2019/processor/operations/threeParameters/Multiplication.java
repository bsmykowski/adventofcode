package year2019.processor.operations.threeParameters;

import year2019.processor.Program;
import year2019.processor.parameters.Parameter;

public class Multiplication extends ThreeParametersOperation {
    public Multiplication(Parameter firstParam, Parameter secondParam, Parameter thirdParam) {
        super(firstParam, secondParam, thirdParam);
    }

    @Override
    public void process(Program program) {
        long val1 = firstParam.getValue(program);
        long val2 = secondParam.getValue(program);
        int destination = thirdParam.getIfDestination(program);
        program.setAt(destination, val1 * val2);
    }
}
