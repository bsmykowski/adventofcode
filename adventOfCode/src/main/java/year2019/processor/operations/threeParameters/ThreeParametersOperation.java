package year2019.processor.operations.threeParameters;

import year2019.processor.operations.Operation;
import year2019.processor.parameters.Parameter;

public abstract class ThreeParametersOperation implements Operation {

    protected Parameter firstParam;
    protected Parameter secondParam;
    protected Parameter thirdParam;

    protected ThreeParametersOperation(Parameter firstParam, Parameter secondParam, Parameter thirdParam) {
        this.firstParam = firstParam;
        this.secondParam = secondParam;
        this.thirdParam = thirdParam;
    }
}
