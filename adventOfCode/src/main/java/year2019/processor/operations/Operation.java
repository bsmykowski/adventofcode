package year2019.processor.operations;

import year2019.processor.Program;

public interface Operation {

    void process(Program program);

}
