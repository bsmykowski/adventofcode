package year2019.processor.operations;

import lombok.Getter;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum OperationType {
    ADDITION(1, 3),
    MULTIPLICATION(2, 3),
    INPUT(3, 1),
    OUTPUT(4, 1),
    JUMP_IF_NOT_ZERO(5, 2),
    JUMP_IF_ZERO(6, 2),
    LESS_THEN(7, 3),
    EQUALS(8, 3),
    RELATIVE_BASE_OFFSET(9, 1);

    private int opCode;
    @Getter
    private int numberOfParameters;

    OperationType(int opCode, int numberOfParameters){
        this.opCode = opCode;
        this.numberOfParameters = numberOfParameters;
    }

    public static OperationType getOperationType(long operationDefinition) {
        return Arrays.stream(OperationType.values())
                .filter(t -> t.opCode == operationDefinition % 100)
                .findFirst()
                .orElseThrow(()->new RuntimeException("No such operation " + operationDefinition));
    }

    public static boolean operationExist(long operationDefinition){
        return !Arrays.stream(OperationType.values())
                .filter(t -> t.opCode == operationDefinition % 100)
                .collect(Collectors.toList())
                .isEmpty();
    }

    public static int getNumberOfParameters(long operationDefinition){
        return getOperationType(operationDefinition).numberOfParameters;
    }

}
