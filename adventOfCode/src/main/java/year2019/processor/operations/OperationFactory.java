package year2019.processor.operations;

import year2019.processor.operations.oneParameter.Input;
import year2019.processor.operations.oneParameter.Output;
import year2019.processor.operations.oneParameter.RelativeBaseOffset;
import year2019.processor.operations.threeParameters.Addition;
import year2019.processor.operations.threeParameters.EqualsTest;
import year2019.processor.operations.threeParameters.LessThenTest;
import year2019.processor.operations.threeParameters.Multiplication;
import year2019.processor.operations.twoParameters.JumpIfNotZero;
import year2019.processor.operations.twoParameters.JumpIfZero;
import year2019.processor.parameters.Parameter;
import year2019.processor.parameters.ParameterFactory;

import java.util.ArrayList;
import java.util.List;

public class OperationFactory {

    public static Operation produce(List<Long> operationData) {

        long operationDefinition = operationData.get(0);

        OperationType operationType = OperationType.getOperationType(operationDefinition);
        int parametersNumber = operationType.getNumberOfParameters();

        List<Parameter> parameters = new ArrayList<>();
        for(int i = 0; i< parametersNumber; i++){
            int mode = (int) (operationDefinition / Math.pow(10, i + 2)) % 10;
            long value = operationData.get(i + 1);
            Parameter parameter = ParameterFactory.produce(mode, value);
            parameters.add(parameter);
        }

        switch(operationType){
            case ADDITION:
                return new Addition(parameters.get(0), parameters.get(1), parameters.get(2));
            case MULTIPLICATION:
                return new Multiplication(parameters.get(0), parameters.get(1), parameters.get(2));
            case INPUT:
                return new Input(parameters.get(0));
            case OUTPUT:
                return new Output(parameters.get(0));
            case JUMP_IF_NOT_ZERO:
                return new JumpIfNotZero(parameters.get(0), parameters.get(1));
            case JUMP_IF_ZERO:
                return new JumpIfZero(parameters.get(0), parameters.get(1));
            case LESS_THEN:
                return new LessThenTest(parameters.get(0), parameters.get(1), parameters.get(2));
            case EQUALS:
                return new EqualsTest(parameters.get(0), parameters.get(1), parameters.get(2));
            case RELATIVE_BASE_OFFSET:
                return new RelativeBaseOffset(parameters.get(0));
        }

        throw new RuntimeException("Cannot produce operation");
    }

}
