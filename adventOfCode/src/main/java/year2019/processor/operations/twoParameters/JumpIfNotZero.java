package year2019.processor.operations.twoParameters;

import year2019.processor.Program;
import year2019.processor.ProgramState;
import year2019.processor.parameters.Parameter;

public class JumpIfNotZero extends TwoParametersOperation {
    public JumpIfNotZero(Parameter firstParam, Parameter secondParam) {
        super(firstParam, secondParam);
    }

    @Override
    public void process(Program program) {
        if(firstParam.getValue(program) != 0){
            program.setOperationPointer((int)secondParam.getValue(program));
            program.setState(ProgramState.JUMPING);
        }
    }
}
