package year2019.processor.operations.twoParameters;

import year2019.processor.operations.Operation;
import year2019.processor.parameters.Parameter;

public abstract class TwoParametersOperation implements Operation {
    protected Parameter firstParam;
    protected Parameter secondParam;

    protected TwoParametersOperation(Parameter firstParam, Parameter secondParam) {
        this.firstParam = firstParam;
        this.secondParam = secondParam;
    }
}
