package year2019.processor.parameters;

import year2019.processor.Program;

public abstract class Parameter {
    protected long value;

    public Parameter(long value){
        this.value = value;
    }

    public abstract long getValue(Program program);

    public abstract int getIfDestination(Program program);

}
