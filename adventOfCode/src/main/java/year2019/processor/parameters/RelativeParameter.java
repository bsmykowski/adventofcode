package year2019.processor.parameters;

import year2019.processor.Program;

public class RelativeParameter extends Parameter {
    public RelativeParameter(long value) {
        super(value);
    }

    @Override
    public long getValue(Program program) {
        return program.getAt(program.getRelativeBase() + (int)value);
    }

    @Override
    public int getIfDestination(Program program) {
        return program.getRelativeBase() + (int)value;
    }
}
