package year2019.processor.parameters;

import java.util.Arrays;

public enum  ParameterMode {
    POSITION(0), IMMEDIATE(1), RELATIVE(2);

    private int code;

    ParameterMode(int code){
        this.code = code;
    }

    public static ParameterMode getParameterMode(int code) {
        return Arrays.stream(ParameterMode.values())
                .filter(t -> t.code == code)
                .findFirst()
                .orElseThrow(()->new RuntimeException("No such mode " + code));
    }
}
