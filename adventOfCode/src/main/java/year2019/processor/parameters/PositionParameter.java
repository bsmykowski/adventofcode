package year2019.processor.parameters;

import year2019.processor.Program;

public class PositionParameter extends Parameter {
    public PositionParameter(long value) {
        super(value);
    }

    @Override
    public long getValue(Program program) {
        return program.getAt((int)value);
    }

    @Override
    public int getIfDestination(Program program) {
        return (int)value;
    }
}
