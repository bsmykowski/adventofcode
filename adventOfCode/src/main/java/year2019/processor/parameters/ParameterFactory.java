package year2019.processor.parameters;

public class ParameterFactory {

    public static Parameter produce(int mode, long value){
        ParameterMode parameterMode = ParameterMode.getParameterMode(mode);

        switch(parameterMode){
            case POSITION:
                return new PositionParameter(value);
            case IMMEDIATE:
                return new ImmediateParameter(value);
            case RELATIVE:
                return new RelativeParameter(value);
        }

        throw new RuntimeException("Cannot produce parameter.");
    }

}
