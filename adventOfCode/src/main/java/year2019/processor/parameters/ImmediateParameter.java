package year2019.processor.parameters;

import year2019.processor.Program;

public class ImmediateParameter extends Parameter {

    public ImmediateParameter(long value) {
        super(value);
    }

    @Override
    public long getValue(Program program) {
        return value;
    }

    @Override
    public int getIfDestination(Program program) {
        return (int) value;
    }
}
