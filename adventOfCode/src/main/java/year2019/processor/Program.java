package year2019.processor;

import lombok.Getter;
import lombok.Setter;
import year2019.processor.operations.Operation;
import year2019.processor.operations.OperationFactory;
import year2019.processor.operations.OperationType;

import java.util.*;

public class Program {

    private Map<Integer, Long> programData;
    @Setter
    private int operationPointer;
    @Getter
    private List<Long> outputBuffer = new ArrayList<>();
    @Getter
    private List<Long> inputBuffer = new ArrayList<>();
    @Getter
    private int inputIndex;
    @Setter
    private ProgramState state;
    @Setter
    @Getter
    private int relativeBase;

    public Program(List<Long> programData){
        this.programData = new HashMap<>();
        for(int i = 0; i < programData.size(); i++){
            this.programData.put(i, programData.get(i));
        }
        operationPointer = 0;
        inputIndex = 0;
        state = ProgramState.BEFORE_START;
        relativeBase = 0;
    }

    public Program(List<Long> programData, List<Long> inputBuffer){
        this(programData);
        this.inputBuffer = inputBuffer;
    }

    public Program(List<Long> programData, long input){
        this(programData);
        this.inputBuffer.add(input);
    }

    public void run(){
        while(!state.equals(ProgramState.END) && !state.equals(ProgramState.WAIT_FOR_INPUT)){
            int numberOfParams = OperationType.getNumberOfParameters(programData.get(operationPointer));
            int sizeOfOperationData = numberOfParams + 1;
            List<Long> operationData = new ArrayList<>();
            for(int i = 0; i < sizeOfOperationData; i++){
                operationData.add(programData.get(operationPointer + i));
            }
            Operation currentOperation = OperationFactory.produce(operationData);
            currentOperation.process(this);
            if(!state.equals(ProgramState.WAIT_FOR_INPUT)) {
                if (!state.equals(ProgramState.JUMPING)) {
                    operationPointer += sizeOfOperationData;
                }
                if (state.equals(ProgramState.JUMPING)) {
                    state = ProgramState.RUNNING;
                }
            }
            if(!OperationType.operationExist(programData.get(operationPointer))){
                state = ProgramState.END;
            }
        }
    }

    public long getAt(int index){
        if(!programData.containsKey(index)){
            programData.put(index, 0L);
        }
        return programData.get(index);
    }

    public void setAt(int index, long value){
        programData.replace(index, value);
        programData.putIfAbsent(index, value);
    }

    public long getFirstOutput(){
        return Objects.requireNonNull(outputBuffer.get(0));
    }

    public long getLastOutput(){
        return Objects.requireNonNull(outputBuffer.get(outputBuffer.size()-1));
    }

    public boolean hasInput(){
        return inputBuffer.size() > inputIndex;
    }

    public long consumeInput(){
        if(!hasInput()){
            state = ProgramState.WAIT_FOR_INPUT;
            return -1;
        }
        long result = inputBuffer.get(inputIndex);
        inputIndex++;
        return result;
    }

    public void addInput(long input){
        inputBuffer.add(input);
        state = ProgramState.RUNNING;
    }

    public boolean isRunning(){
        return state.equals(ProgramState.RUNNING)
                || state.equals(ProgramState.JUMPING)
                || state.equals(ProgramState.WAIT_FOR_INPUT);
    }
}
