package year2019.day6Orbits;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day6 {

    public static void main(String[] args) {

        String path = GlobalConstatns.getPath(2019, "day6Orbits");
        List<String> lines = FilesReader.getLines(path);

        Map<String, MassObject> objects = new HashMap<>();

        for(String line : lines){
            String[] objectIds = line.split("\\)");
            for(String objectId: objectIds){
                objects.putIfAbsent(objectId, new MassObject(objectId));
            }
            objects.get(objectIds[0]).addOrbitingObject(objects.get(objectIds[1]));
            objects.get(objectIds[1]).setOrbitsAround(objects.get(objectIds[0]));
        }

        MassObject mainObject = objects
                                    .values()
                                    .stream()
                                    .filter(o -> o.getOrbitsAround() == null)
                                    .collect(Collectors.toList())
                                    .get(0);

        MassObject santaObject = objects
                .values()
                .stream()
                .filter(o -> o.getId().equals("SAN"))
                .collect(Collectors.toList())
                .get(0);

        mainObject.print();
        System.out.println(santaObject.distance("YOU")-2);

    }

}
