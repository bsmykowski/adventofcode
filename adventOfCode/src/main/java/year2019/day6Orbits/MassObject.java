package year2019.day6Orbits;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class MassObject {
    private MassObject orbitsAround;
    private List<MassObject> objectsAroundThis = new ArrayList<>();
    private String id;

    public MassObject(String id){
        this.id = id;
    }

    public void addOrbitingObject(MassObject massObject){
        objectsAroundThis.add(massObject);
    }

    public void print(){
        print(0);
    }

    public void print(int depth) {
        for(int i = 0; i < depth; i++){
            System.out.print("   ");
        }
        System.out.println(id);
        for(MassObject satellite: objectsAroundThis){
            satellite.print(depth+1);
        }
    }

    public int getOrbitsAmount(){
        return getOrbitsAmount(0);
    }

    public int getOrbitsAmount(int depth){
        int result = 0;
        for(MassObject satellite: objectsAroundThis){
            result += satellite.getOrbitsAmount(depth+1)+depth+1;
        }
        return result;
    }

    public int distance(String destination){
        return distance(destination, "", 1);
    }

    public int distance(String destination, String cameFrom, int distance){
        int minDist = Integer.MAX_VALUE;

        List<MassObject> possibleNextMoves = new ArrayList<>();
        if(orbitsAround != null)
            possibleNextMoves.add(orbitsAround);
        possibleNextMoves.addAll(objectsAroundThis);
        possibleNextMoves = possibleNextMoves
                                .stream()
                                .filter(o -> !o.getId().equals(cameFrom))
                                .collect(Collectors.toList());

        for(MassObject satellite: possibleNextMoves){
            if(satellite.getId().equals(destination)){
                return distance;
            }
            int dist = satellite.distance(destination, this.id, distance+1);
            if(dist < minDist){
                minDist = dist;
            }
        }

        return minDist;
    }
}
