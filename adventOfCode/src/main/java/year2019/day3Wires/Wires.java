package year2019.day3Wires;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class Wires {
    private Map<Point, List<WireElement>> wires;
    private Point downRight;
    private Point topLeft;
    private Point closestCross;

    public Wires(List<String> lines){
        closestCross = new Point(Integer.MAX_VALUE/3, Integer.MAX_VALUE/3);
        wires = new HashMap<>();
        downRight = new Point(0,0);
        topLeft = new Point(0,0);
        int lineId = 0;
        for(String line :lines){
            Point actualPoint = new Point(0,0);
            int distanceFromStart = 0;
            wires.computeIfAbsent(actualPoint, k -> new ArrayList<>());
            wires.get(actualPoint).add(new WireElement(actualPoint, lineId, distanceFromStart));
            String[] wireLines = line.split(",");
            for(String wireLine : wireLines){
                Character direction = wireLine.charAt(0);
                int distance = Integer.parseInt(wireLine.substring(1));
                for(int i = 0; i < distance; i++){
                    actualPoint = actualPoint.getNextTo(Direction.getDirection(direction));
                    distanceFromStart++;
                    wires.computeIfAbsent(actualPoint, k -> new ArrayList<>());
                    wires.get(actualPoint).add(new WireElement(actualPoint, lineId, distanceFromStart));
                    if(wires.get(actualPoint).size() > 1
                            && wires.get(actualPoint).get(0).getLineId() != wires.get(actualPoint).get(1).getLineId()){
                        wires.get(actualPoint).add(new Cross(actualPoint, lineId));
                        if(actualPoint.distance(new Point(0,0)) <= closestCross.distance(new Point(0,0))){
                            closestCross = new Point(actualPoint.getX(), actualPoint.getY());
                        }
                    }
                    if(actualPoint.getX() > downRight.getX()){
                        downRight.setX(actualPoint.getX());
                    }
                    if(actualPoint.getY() > downRight.getY()){
                        downRight.setY(actualPoint.getY());
                    }
                    if(actualPoint.getX() < topLeft.getX()){
                        topLeft.setX(actualPoint.getX());
                    }
                    if(actualPoint.getY() < topLeft.getY()){
                        topLeft.setY(actualPoint.getY());
                    }
                }
            }
            lineId++;

        }
    }

    public int getClosestIntersection(){
        int bestDistance = Integer.MAX_VALUE;
        for(Point key : wires.keySet()){
            List<WireElement> wireElements = wires.get(key);
            if(wireElements.size() > 1){
                for (WireElement wireElement : wireElements){
                    if(wireElement instanceof Cross){
                        int distance = wireElements.stream().mapToInt(WireElement::getDistanceFromStart).reduce((e1, e2) -> e1+e2).getAsInt();
                        if(distance < bestDistance){
                            bestDistance = distance;
                        }
                    }
                }
            }
        }
        return bestDistance;
    }

    public List<WireElement> getWires(){
        List<WireElement> result = new ArrayList<>();
        for(List<WireElement> wireElements: wires.values())
            result.addAll(wireElements);
        return result;
    }

    public int getWidth(){
        return downRight.getX() - topLeft.getX() + 1;
    }

    public int getHeight(){
        return downRight.getY() - topLeft.getY() + 1;
    }

}
