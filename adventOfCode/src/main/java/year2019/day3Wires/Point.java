package year2019.day3Wires;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Point {
    private int x;
    private int y;

    public Point getNextTo(Direction direction){
        switch(direction){
            case LEFT:
                return new Point(x-1, y);
            case RIGHT:
                return new Point(x+1, y);
            case UP:
                return new Point(x, y-1);
            case DOWN:
                return new Point(x, y+1);
            default:
                return new Point(x, y);
        }
    }

    public int distance(Point point){
        return Math.abs(point.getX()-x) + Math.abs(point.getY()-y);
    }
}
