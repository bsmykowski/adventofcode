package year2019.day3Wires;

import javafx.scene.paint.Color;

public class Cross extends WireElement {

    public Cross(Point point, int lineId) {
        super(point, lineId, 0);
        setColor(Color.RED);
    }
}
