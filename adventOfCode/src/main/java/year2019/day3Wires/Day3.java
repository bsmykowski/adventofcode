package year2019.day3Wires;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import util.FilesReader;
import util.GlobalConstatns;

import java.util.List;

public class Day3 extends Application {
    private static final double SCALE = .1;
    private static final double SCENE_HEIGHT = 1000;
    private static final double SCENE_WIDTH = 1000;

    @Override
    public void start(Stage primaryStage) throws Exception {

        String path = GlobalConstatns.getPath(2019, "day3Wires");
        List<String> lines = FilesReader.getLines(path);

        Wires wires = new Wires(lines);

        int width = wires.getWidth();
        int height = wires.getHeight();

        AnchorPane root = new AnchorPane();
        Canvas canvas = new Canvas(width * SCALE, height * SCALE);
        ScrollPane scrollPane = new ScrollPane(root);
        scrollPane.setHvalue(0);
        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(scrollPane, SCENE_WIDTH, SCENE_HEIGHT));
        primaryStage.show();

        System.out.println(wires.getClosestCross().distance(new Point(0,0)));
        System.out.println(wires.getClosestIntersection());

        drawCanvas(wires, canvas);
    }

    private void drawCanvas(Wires wires, Canvas canvas) {
        WiresDrawer wiresDrawer = new WiresDrawer(wires.getHeight(), wires.getWidth(), SCALE);
        GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
        wiresDrawer.draw(wires, graphicsContext2D);
    }

    public static void main(String[] args) {
        launch(args);
    }
}