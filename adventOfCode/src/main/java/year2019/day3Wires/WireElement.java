package year2019.day3Wires;

import javafx.scene.paint.Color;
import lombok.Data;

@Data
public class WireElement {

    private Point position;
    private Color color;
    private int lineId;
    private int distanceFromStart;

    public WireElement(Point point, int lineId, int distanceFromStart){
        position = point;
        this.distanceFromStart = distanceFromStart;
        color = Color.SILVER;
        this.lineId = lineId;
    }

    public int getX(){
        return position.getX();
    }

    public int getY(){
        return position.getY();
    }

}
