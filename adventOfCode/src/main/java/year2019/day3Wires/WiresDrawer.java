package year2019.day3Wires;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class WiresDrawer {

    private double height;
    private double width;
    private double scale;

    public void draw(Wires wires, GraphicsContext graphicsContext2D){

        graphicsContext2D.setFill(Color.WHITE);
        graphicsContext2D.fillRect(0, 0, width*scale, height*scale);

        for(WireElement wireElement :wires.getWires()){
            drawWireElement(graphicsContext2D, wireElement, wires.getTopLeft());
        }

        drawFilledRect(graphicsContext2D, -wires.getTopLeft().getX()*scale, -wires.getTopLeft().getY()*scale, Color.GREEN);
    }

    private void drawWireElement(GraphicsContext graphicsContext2D, WireElement wireElement, Point topLeftPosition) {
        double x = (wireElement.getX() - topLeftPosition.getX()) * scale;
        double y = (wireElement.getY() - topLeftPosition.getY()) * scale;
        drawFilledRect(graphicsContext2D, x, y, wireElement.getColor());
    }

    private void drawFilledRect(GraphicsContext graphicsContext2D, double x, double y, Color color) {
        graphicsContext2D.setFill(color);
        double rectSize = scale < 1 ? 1 : scale;
        graphicsContext2D.fillRect(x, y, rectSize, rectSize);
    }

}
