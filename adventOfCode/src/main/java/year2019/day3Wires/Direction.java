package year2019.day3Wires;

public enum Direction {
    LEFT, RIGHT, UP, DOWN, NONE;

    public static Direction getDirection(Character character){
        switch(character){
            case 'D':
                return DOWN;
            case 'U':
                return UP;
            case 'L':
                return LEFT;
            case 'R':
                return RIGHT;
        }
        return NONE;
    }
}
