package year2019.day5Processor2;

import util.FilesReader;
import util.GlobalConstatns;
import year2019.processor.Program;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day5 {

    public static void main(String[] args) {
        String path = GlobalConstatns.getPath(2019, "day5Processor2");
        List<String> lines = FilesReader.getLines(path);

        List<Long> instructions = Arrays.stream(lines.get(0).split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList());


        List<Long> programData = new ArrayList<>(instructions);

        int input = 5;

        Program program = new Program(programData, input);
        program.run();
        System.out.println(program.getOutputBuffer());
        System.out.println("Should be 9217546");

    }


}
