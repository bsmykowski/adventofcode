package year2019.day2ProcessorBasic;

import util.FilesReader;
import util.GlobalConstatns;
import year2019.processor.Program;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day2 {

    public static void main(String[] args) {

        String path = GlobalConstatns.getPath(2019, "day2ProcessorBasic");
        List<String> lines = FilesReader.getLines(path);

        List<Long> instructions = Arrays.stream(lines.get(0).split(","))
                                            .map(Long::parseLong)
                                            .collect(Collectors.toList());


        for(int i = 0; i < 100; i++){
            for(int j = 0; j < 100; j++){
                List<Long> programData = new ArrayList<>(instructions);

                long noun = i;
                long verb = j;

                programData.set(1, noun);
                programData.set(2, verb);

                Program program = new Program(programData);
                program.run();
                if(program.getAt(0) == 19690720){
                    System.out.println(100*noun + verb);
                    System.out.println("Should be 6718");
                }

            }
        }

    }
}
