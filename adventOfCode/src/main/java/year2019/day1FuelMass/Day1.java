package year2019.day1FuelMass;

import util.FilesReader;
import util.GlobalConstatns;

import java.util.List;

public class Day1 {

    public static void main(String[] args) {

        String path = GlobalConstatns.getPath(2019, "day1FuelMass");
        List<String> lines = FilesReader.getLines(path);

        Integer requirements = lines.stream().map(l->{
            int mass = Integer.parseInt(l);
            int fuel = 0;
            int partFuel = mass/3-2;
            while (partFuel > 0){
                fuel += partFuel;
                partFuel = partFuel/3-2;
            }
            return fuel;
        }).reduce((r1,r2) -> r1+r2).get();

        System.out.println(requirements);

    }

}
